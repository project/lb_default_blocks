<?php

namespace Drupal\Tests\lb_default_blocks\Kernel;

use Drupal\lb_default_blocks\Entity\LayoutPosition;

/**
 * Defines a class for testing position entity.
 *
 * @group lb_default_blocks
 * @covers \Drupal\lb_default_blocks\Entity\LayoutPosition
 */
class PositionEntityTest extends LbDefaultBlocksKernelTestBase {

  /**
   * Tests storage and retrieval.
   */
  public function testStorageAndRetrieval() {
    $id = $this->randomMachineName();
    $name = $this->randomMachineName();
    $layout = $this->createLayoutPosition([
      'id' => $id,
      'name' => $name,
    ]);
    $this->assertEquals($id, $layout->id());
    $this->assertEquals($name, $layout->label());
    $reloaded = \Drupal::entityTypeManager()->getStorage('lb_default_blocks_position')->loadUnchanged($id);
    $this->assertEquals($name, $reloaded->label());
  }

  /**
   * Tests layout position label access.
   */
  public function testLayoutPositionLabelAccess() {
    $user = $this->createUser(['access content']);
    $position = LayoutPosition::create([
      'id' => $this->randomMachineName(),
      'name' => $this->randomMachineName(),
    ]);
    $this->assertTrue($position->access('view label', $user));
  }

  /**
   * Tests config dependencies.
   */
  public function testLayoutPositionDependencies() {
    $type = $this->enableDefaultBlockSupportForBlockType($this->createBlockContentType('text', 'Text'));
    $this->enableDefaultBlockSupportForBlockType($this->createBlockContentType('stuff', 'Stuff'));
    $block_content = $this->createBlockContent(['type' => 'text']);
    $position1 = $this->createLayoutPosition([
      'id' => 'position1',
      'default' => $block_content->uuid(),
      'filterSettings' => [
        [
          'id' => 'block_type',
          'enabled' => TRUE,
          'weight' => 0,
          'settings' => [
            'block_types' => ['text', 'stuff'],
          ],
        ],
      ],
    ]);
    $dependencies = $position1->calculateDependencies()->getDependencies();
    $this->assertEquals([
      'config' => [
        // Alphabetically sorted.
        'block_content.type.stuff',
        'block_content.type.text',
      ],
      'content' => [
        'block_content:text:' . $block_content->uuid(),
      ],
    ], $dependencies);
    $type->delete();
    $position1 = \Drupal::entityTypeManager()->getStorage('lb_default_blocks_position')->loadUnchanged($position1->id());
    $dependencies = $position1->calculateDependencies()->getDependencies();
    $this->assertEquals([
      'config' => [
        'block_content.type.stuff',
      ],
      'content' => [
        'block_content:text:' . $block_content->uuid(),
      ],
    ], $dependencies);
    $this->assertEquals([
      [
        'id' => 'block_type',
        'enabled' => TRUE,
        'weight' => 0,
        'settings' => [
          'block_types' => ['stuff'],
        ],
      ],
    ], $position1->get('filterSettings'));
  }

}
