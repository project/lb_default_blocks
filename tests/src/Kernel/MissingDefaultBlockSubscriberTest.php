<?php

namespace Drupal\KernelTests\Core\Config;

use Drupal\Core\Config\ConfigImporter;
use Drupal\Core\Config\StorageComparer;
use Drupal\lb_default_blocks\Hooks\BlockContentTypeIntegration;
use Drupal\Tests\lb_default_blocks\Kernel\LbDefaultBlocksKernelTestBase;

/**
 * Tests importing configuration which has missing content dependencies.
 *
 * @group lb_default_blocks
 * @covers \Drupal\lb_default_blocks\EventSubscriber\MissingDefaultBlockSubscriber
 */
class MissingDefaultBlockSubscriberTest extends LbDefaultBlocksKernelTestBase {

  /**
   * Config Importer object used for testing.
   *
   * @var \Drupal\Core\Config\ConfigImporter
   */
  protected $configImporter;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['system']);
    $this->copyConfig($this->container->get('config.storage'), $this->container->get('config.storage.sync'));

    // Set up the ConfigImporter object for testing.
    $storage_comparer = new StorageComparer(
      $this->container->get('config.storage.sync'),
      $this->container->get('config.storage')
    );
    $this->configImporter = new ConfigImporter(
      $storage_comparer->createChangelist(),
      $this->container->get('event_dispatcher'),
      $this->container->get('config.manager'),
      $this->container->get('lock'),
      $this->container->get('config.typed'),
      $this->container->get('module_handler'),
      $this->container->get('module_installer'),
      $this->container->get('theme_handler'),
      $this->container->get('string_translation'),
      $this->container->get('extension.list.module')
    );
  }

  /**
   * Tests that missing blocks are created if they're missing.
   */
  public function testMissingContent() {
    $storage = $this->container->get('config.storage');
    $this->enableDefaultBlockSupportForBlockType($this->createBlockContentType('text', 'Text'));
    \Drupal::service('entity_field.manager')->clearCachedFieldDefinitions();
    $block = $this->createBlockContent(['type' => 'text']);
    $position = $this->createLayoutPosition();
    $sync = $this->container->get('config.storage.sync');
    $item = $storage->read($position->getConfigDependencyName());
    $this->assertEmpty($item['dependencies']['content'] ?? []);
    $item['dependencies']['content'] = ['block_content:text:' . $block->uuid()];
    $sync->write($position->getConfigDependencyName(), $item);
    $this->container->get('config.import_transformer')->transform($sync);
    $block->delete();
    $this->assertEmpty(\Drupal::service('entity.repository')->loadEntityByUuid('block_content', $block->uuid()));

    // Import.
    $this->configImporter->reset()->import();
    $this->assertEquals([], $this->configImporter->getErrors(), 'There were no errors during the import.');

    $item = $storage->read($position->getConfigDependencyName());
    $loaded = \Drupal::service('entity.repository')->loadEntityByUuid('block_content', $block->uuid());
    $this->assertNotEmpty($loaded);
    $this->assertEquals(['block_content:text:' . $block->uuid()], $item['dependencies']['content']);
    $this->assertEquals($position->id(), $loaded->{BlockContentTypeIntegration::FIELD_NAME}->target_id);
  }

}
