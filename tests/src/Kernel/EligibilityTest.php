<?php

namespace Drupal\Tests\lb_default_blocks\Kernel;

use Drupal\block_content\BlockContentInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\lb_default_blocks\Eligibility;
use Drupal\lb_default_blocks\Hooks\BlockContentTypeIntegration;

/**
 * Defines a class for testing eligibility service.
 *
 * @group lb_default_blocks
 * @covers \Drupal\lb_default_blocks\Eligibility
 */
class EligibilityTest extends LbDefaultBlocksKernelTestBase {

  /**
   * Tests eligibility.
   */
  public function testEligibility() {
    $type = $this->createBlockContentType('text', 'Text');
    $eligibility = \Drupal::service('lb_default_blocks.eligibility');
    assert($eligibility instanceof Eligibility);
    $block = $this->createBlockContent(['type' => 'text']);
    $this->assertFalse($eligibility->blockContentIsEligibleAsDefault($block));
    $this->enableDefaultBlockSupportForBlockType($type);
    $this->assertFalse($eligibility->blockContentIsEligibleAsDefault($block));

    $position = $this->createLayoutPosition();
    $field_manager = \Drupal::service('entity_field.manager');
    assert($field_manager instanceof EntityFieldManagerInterface);
    $field_manager->clearCachedFieldDefinitions();
    $block = \Drupal::entityTypeManager()->getStorage('block_content')->loadUnchanged($block->id());
    assert($block instanceof BlockContentInterface);
    $block->{BlockContentTypeIntegration::FIELD_NAME} = $position;
    $block->save();
    $this->assertTrue($eligibility->blockContentIsEligibleAsDefault($block));
  }

}
