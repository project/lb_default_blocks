<?php

namespace Drupal\Tests\lb_default_blocks\Kernel;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\lb_default_blocks\Hooks\BlockContentTypeIntegration;

/**
 * Defines a class for testing block content type hooks.
 *
 * @group lb_default_blocks
 * @covers \Drupal\lb_default_blocks\Hooks\BlockContentTypeIntegration
 */
class BlockContentTypeHooksTest extends LbDefaultBlocksKernelTestBase {

  /**
   * Tests block content type hooks.
   */
  public function testBlockContentTypeHooks() {
    $type = $this->createBlockContentType('text', 'Text');
    $field_manager = \Drupal::service('entity_field.manager');
    assert($field_manager instanceof EntityFieldManagerInterface);
    $this->assertArrayNotHasKey(BlockContentTypeIntegration::FIELD_NAME, $field_manager->getFieldDefinitions('block_content', 'text'));
    $this->enableDefaultBlockSupportForBlockType($type);
    $field_manager->clearCachedFieldDefinitions();
    $this->assertArrayHasKey(BlockContentTypeIntegration::FIELD_NAME, $field_manager->getFieldDefinitions('block_content', 'text'));
    $type->setThirdPartySetting('lb_default_blocks', 'enabled', FALSE)->save();
    $field_manager->clearCachedFieldDefinitions();
    $this->assertArrayNotHasKey(BlockContentTypeIntegration::FIELD_NAME, $field_manager->getFieldDefinitions('block_content', 'text'));
  }

}
