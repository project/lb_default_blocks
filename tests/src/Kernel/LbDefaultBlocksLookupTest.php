<?php

namespace Drupal\Tests\lb_default_blocks\Kernel;

use Drupal\lb_default_blocks\Hooks\BlockContentTypeIntegration;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Defines a class for testing default lookup.
 *
 * @group lb_default_blocks
 * @covers \Drupal\lb_default_blocks\Entity\LayoutPosition
 */
class LbDefaultBlocksLookupTest extends LbDefaultBlocksKernelTestBase {

  use TaxonomyTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'lb_default_blocks',
    'node',
    'block_content',
    'field',
    'filter',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();
    $this->enableDefaultBlockSupportForBlockType($this->createBlockContentType('text', 'Text'));
    $this->enableDefaultBlockSupportForBlockType($this->createBlockContentType('stuff', 'Stuff'));
  }

  /**
   * Tests block lookup.
   */
  public function testDefaultBlockLookup() {
    $position1 = $this->createLayoutPosition([
      'id' => 'position1',
      'filterSettings' => [
        [
          'id' => 'block_type',
          'enabled' => TRUE,
          'weight' => 0,
          'settings' => [
            'block_types' => ['text'],
          ],
        ],
      ],
    ]);
    $this->createLayoutPosition(['id' => 'position2']);
    // Note no contexts, items or position.
    $this->createBlockContent([
      'type' => 'text',
      'label' => $this->randomMachineName(),
    ]);
    $this->createBlockContent([
      'type' => 'stuff',
      'label' => $this->randomMachineName(),
    ]);
    $lookup = \Drupal::service('lb_default_blocks.lookup');

    $node = $this->createNode([]);

    // Red herrings (don't match on position or type, should not be found).
    $this->createBlockContent([
      'type' => 'text',
      'label' => $this->randomMachineName(),
      BlockContentTypeIntegration::FIELD_NAME => ['target_id' => 'position2'],
    ]);
    $this->createBlockContent([
      'type' => 'stuff',
      'label' => $this->randomMachineName(),
      BlockContentTypeIntegration::FIELD_NAME => ['target_id' => 'position1'],
    ]);
    $this->createBlockContent([
      'type' => 'stuff',
      'label' => $this->randomMachineName(),
      BlockContentTypeIntegration::FIELD_NAME => ['target_id' => 'position2'],
    ]);
    // Red-herring, not published.
    $this->createBlockContent([
      'type' => 'text',
      'label' => $this->randomMachineName(),
      'status' => 0,
      BlockContentTypeIntegration::FIELD_NAME => ['target_id' => 'position1'],
    ]);
    // Test that items without a position are not found.
    $this->assertLookup([], $lookup->lookupDefaultBlocksForEntity($position1, $node));

    // Test that we can match on context.
    $default = $this->createBlockContent([
      'type' => 'text',
      'label' => $this->randomMachineName(),
      BlockContentTypeIntegration::FIELD_NAME => ['target_id' => 'position1'],
    ]);
    $this->assertLookup([$default], $lookup->lookupDefaultBlocksForEntity($position1, $node));
  }

}
