<?php

declare(strict_types=1);

namespace Drupal\Tests\lb_default_blocks\Kernel;

use Drupal\Core\Entity\EntityInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\lb_default_blocks\Traits\BlockContentTestTrait;
use Drupal\Tests\lb_default_blocks\Traits\LayoutPositionTestTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Defines a base kernel test class for layout-builder default block module.
 */
abstract class LbDefaultBlocksKernelTestBase extends KernelTestBase {

  use LayoutPositionTestTrait;
  use UserCreationTrait;
  use BlockContentTestTrait;
  use NodeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'lb_default_blocks',
    'block',
    'field',
    'node',
    'system',
    'user',
    'text',
    'filter',
    'block_content',
  ];

  /**
   * Operates after initial setup.
   *
   * Extension point for sub-classes that need to make changes before any entity
   * types are installed.
   */
  protected function afterInitialSetup(): void {
    // Nil-op.
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();
    $this->afterInitialSetup();
    $this->installEntitySchema('user');
    $this->setUpCurrentUser();
    $this->installConfig(['filter', 'node']);
    $this->installEntitySchema('node');
    $this->installSchema('node', ['node_access']);
    $this->installEntitySchema('block_content');
  }

  /**
   * Maps entity IDs.
   *
   * @param \Drupal\Core\Entity\EntityInterface[] $entities
   *   Entities to map IDs of.
   *
   * @return string[]
   *   Mapped entity IDs in form {entity_type_id}:{entity_id}
   */
  protected static function mapEntityIds(array $entities): array {
    return array_values(array_map(function (EntityInterface $entity) {
      return sprintf('%s:%s', $entity->getEntityTypeId(), $entity->id());
    }, $entities));
  }

  /**
   * Assert lookup.
   *
   * @param array $expected
   *   Expected entities.
   * @param array $actual
   *   Actual entities.
   */
  protected function assertLookup(array $expected, array $actual) {
    $this->assertEquals($this->mapEntityIds($expected), $this->mapEntityIds($actual));
  }

}
