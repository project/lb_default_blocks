<?php

declare(strict_types=1);

namespace Drupal\Tests\lb_default_blocks\Kernel;

use Drupal\Core\Block\BlockManager;
use Drupal\Core\Render\Element;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\lb_default_blocks\Plugin\Block\DefaultBlock;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Defines a class for testing missing position.
 *
 * @group lb_default_blocks
 */
final class MissingPositionTest extends LbDefaultBlocksKernelTestBase {

  use NodeCreationTrait;

  /**
   * Tests missing position.
   */
  public function testMissingPositionBuild() {
    $manager = \Drupal::service('plugin.manager.block');
    assert($manager instanceof BlockManager);
    $block = $manager->createInstance('lb_default_blocks', [
      'position' => 'yeah_this_position_does_not_exist',
    ]);
    assert($block instanceof DefaultBlock);
    $block->setContextValue('entity', $this->createNode());
    $build = $block->build();
    $this->assertTrue(!empty($build['#cache']));
    $this->assertTrue(Element::isEmpty($build));
  }

  /**
   * Tests missing position.
   */
  public function testMissingPositionAccess() {
    $manager = \Drupal::service('plugin.manager.block');
    assert($manager instanceof BlockManager);
    $block = $manager->createInstance('lb_default_blocks', [
      'position' => 'yeah_this_position_does_not_exist',
    ]);
    assert($block instanceof DefaultBlock);
    $block->setContextValue('entity', $this->createNode());
    $this->assertFalse($block->access(new AnonymousUserSession()));
  }

}
