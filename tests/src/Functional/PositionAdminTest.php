<?php

namespace Drupal\Tests\lb_default_blocks\Functional;

use Drupal\backfill_formatter\BackFillTerms;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\lb_default_blocks\Traits\BlockContentTestTrait;
use Drupal\Tests\lb_default_blocks\Traits\LayoutPositionTestTrait;
use Drupal\lb_default_blocks\Entity\LayoutPosition;
use Drupal\lb_default_blocks\Entity\LayoutPositionInterface;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Defines a class for testing layout-builder default block position entity.
 *
 * @group lb_default_blocks
 * @covers \Drupal\lb_default_blocks\Form\LayoutPositionForm
 * @covers \Drupal\lb_default_blocks\EntityHandlers\LayoutPositionListBuilder
 */
class PositionAdminTest extends BrowserTestBase {

  use TaxonomyTestTrait;
  use LayoutPositionTestTrait;
  use BlockContentTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'seven';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'lb_default_blocks',
    'lb_default_blocks_common_terms',
    'taxonomy',
    'backfill_formatter',
    'taxonomy_entity_index',
    'block_content',
  ];

  /**
   * User interface.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Array of terms.
   *
   * @var \Drupal\taxonomy\TermInterface[]
   */
  protected $terms = [];

  /**
   * Vocabularies.
   *
   * @var \Drupal\taxonomy\VocabularyInterface[]
   */
  protected $vocabularies = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();
    $this->adminUser = $this->createUser([
      'administer lb default block position entities',
      'access administration pages',
    ]);
    $this->vocabularies[] = $vocab1 = $this->createVocabulary();
    $this->vocabularies[] = $vocab2 = $this->createVocabulary();
    foreach ([$vocab1, $vocab2] as $vocab) {
      foreach (range(0, 2) as $delta) {
        $this->terms[] = $this->createTerm($vocab);
      }
    }
    $this->drupalPlaceBlock('local_actions_block', ['region' => 'pre_content']);
  }

  /**
   * Tests layout position administration.
   */
  public function testLayoutPositionAdministration() {
    $this->assertThatAnonymousUserCannotAdministerLayoutPositions();
    $position = $this->assertThatAdminCanAddLayoutPositions();
    $position = $this->assertThatAdminCanEditLayoutPositions($position);
    $this->assertThatAdminCanDeleteLayoutPositions($position);
  }

  /**
   * Tests anonymous users can't access admin routes.
   */
  private function assertThatAnonymousUserCannotAdministerLayoutPositions() : void {
    $position = $this->createLayoutPosition([
      'id' => 'a' . $this->randomMachineName(),
      'name' => 'a' . $this->randomMachineName(),
    ]);
    $urls = [
      Url::fromRoute('entity.lb_default_blocks_position.collection'),
      $position->toUrl('edit-form'),
      $position->toUrl('delete-form'),
    ];
    foreach ($urls as $url) {
      $this->drupalGet($url);
      $this->assertSession()->statusCodeEquals(403);
    }
  }

  /**
   * Assert that admin can add a layout position.
   *
   * @return \Drupal\lb_default_blocks\Entity\LayoutPositionInterface
   *   The added position.
   */
  private function assertThatAdminCanAddLayoutPositions() : LayoutPositionInterface {
    $this->drupalLogin($this->adminUser);
    $block_type1 = $this->enableDefaultBlockSupportForBlockType($this->createBlockContentType($this->randomMachineName()));
    $block_type2 = $this->enableDefaultBlockSupportForBlockType($this->createBlockContentType($this->randomMachineName()));
    $this->addTermReferenceFieldToBlockContentType($block_type1);
    $this->addTermReferenceFieldToBlockContentType($block_type2, 'another_field');

    $this->drupalGet(Url::fromRoute('system.admin_structure'));
    $assert = $this->assertSession();
    $assert->linkExists('Layout positions');
    $this->drupalGet(Url::fromRoute('entity.lb_default_blocks_position.collection'));
    $assert->statusCodeEquals(200);
    $assert->linkExists('Add layout position');
    $this->clickLink('Add layout position');
    $this->assertStringContainsString(Url::fromRoute('entity.lb_default_blocks_position.add_form')->toString(), $this->getSession()->getCurrentUrl());
    $position_name = 'a' . $this->randomMachineName();
    $id = mb_strtolower('a' . $this->randomMachineName());
    $vocab1 = reset($this->vocabularies);
    $vocab2 = end($this->vocabularies);
    $block = $this->createBlockContent(['type' => $block_type1->id()]);
    $this->submitForm([
      'id' => $id,
      'name' => $position_name,
      'description' => $this->randomString(),
      'default' => sprintf('%s (%s)', $block->label(), $block->id()),
      'enabled_filters[block_type][enabled]' => TRUE,
      'enabled_filters[block_type][weight]' => 2,
      'enabled_filters[common_terms][enabled]' => TRUE,
      'enabled_filters[common_terms][weight]' => 1,
      sprintf('filterSettings[block_type][settings][block_types][%s]', $block_type1->id()) => TRUE,
      sprintf('filterSettings[block_type][settings][block_types][%s]', $block_type2->id()) => TRUE,
      sprintf('filterSettings[common_terms][settings][vocabularies][%s][enabled]', $vocab1->id()) => TRUE,
      sprintf('filterSettings[common_terms][settings][vocabularies][%s][weight]', $vocab1->id()) => 1,
      sprintf('filterSettings[common_terms][settings][vocabularies][%s][enabled]', $vocab2->id()) => TRUE,
      sprintf('filterSettings[common_terms][settings][vocabularies][%s][weight]', $vocab2->id()) => 2,
    ], 'Save');
    $assert->pageTextContains(sprintf('Layout position %s has been added.', $position_name));
    $assert->linkExists($position_name);
    $layoutPosition = LayoutPosition::load($id);
    $block_types = [$block_type1->id(), $block_type2->id()];
    sort($block_types);
    $this->assertEquals([
      [
        'id' => 'block_type',
        'enabled' => TRUE,
        'settings' => [
          'block_types' => $block_types,
        ],
        'weight' => 2,
      ],
      [
        'id' => 'common_terms',
        'enabled' => TRUE,
        'weight' => 1,
        'settings' => [
          'vocabularies' => [
            $vocab1->id() => [
              'enabled' => TRUE,
              'weight' => 1,
            ],
            $vocab2->id() => [
              'enabled' => TRUE,
              'weight' => 2,
            ],
          ],
          'backfill_mode' => BackFillTerms::MODE_MATCH_ALL,
        ],
      ],
    ], array_values($layoutPosition->get('filterSettings')));
    $this->assertEquals($block->uuid(), $layoutPosition->getDefault());
    return $layoutPosition;
  }

  /**
   * Assert that admin can edit positions.
   *
   * @param \Drupal\lb_default_blocks\Entity\LayoutPositionInterface $position
   *   Position to edit.
   *
   * @return \Drupal\lb_default_blocks\Entity\LayoutPositionInterface
   *   The edited position.
   */
  private function assertThatAdminCanEditLayoutPositions(LayoutPositionInterface $position) : LayoutPositionInterface {
    $this->drupalGet(Url::fromRoute('entity.lb_default_blocks_position.collection'));
    $assert = $this->assertSession();
    $edit = $position->toUrl('edit-form');
    $assert->linkByHrefExists($edit->toString());
    $this->drupalGet($edit);
    $assert->fieldValueEquals('name', $position->label());
    $assert->fieldValueEquals('description', $position->getDescription());
    $new_name = 'a' . $this->randomMachineName();
    $this->submitForm([
      'name' => $new_name,
    ], 'Save');
    $assert->pageTextContains(sprintf('Layout position %s has been updated.', $new_name));
    $position = \Drupal::entityTypeManager()->getStorage('lb_default_blocks_position')->loadUnchanged($position->id());
    assert($position instanceof LayoutPositionInterface);
    return $position;
  }

  /**
   * Assert that admin can delete layout positions.
   *
   * @param \Drupal\lb_default_blocks\Entity\LayoutPositionInterface $position
   *   The position to delete.
   */
  private function assertThatAdminCanDeleteLayoutPositions(LayoutPositionInterface $position) : void {
    $this->drupalGet(Url::fromRoute('entity.lb_default_blocks_position.collection'));
    $assert = $this->assertSession();
    $delete = $position->toUrl('delete-form');
    $assert->linkByHrefExists($delete->toString());
    $this->drupalGet($delete);
    $this->submitForm([], 'Delete');
    $assert->pageTextContains(sprintf('The layout position %s has been deleted.', $position->label()));
  }

}
