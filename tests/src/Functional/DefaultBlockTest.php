<?php

namespace Drupal\Tests\lb_default_blocks\Functional;

use Drupal\backfill_formatter\BackFillTerms;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\lb_default_blocks\Hooks\BlockContentTypeIntegration;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionComponent;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\lb_default_blocks\Traits\BlockContentTestTrait;
use Drupal\Tests\lb_default_blocks\Traits\LayoutPositionTestTrait;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Defines a class for testing the layout-builder default block.
 *
 * @group lb_default_blocks
 */
class DefaultBlockTest extends BrowserTestBase {

  use BlockContentTestTrait;
  use LayoutPositionTestTrait;
  use TaxonomyTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'lb_default_blocks',
    'node',
    'block_content',
    'field',
    'filter',
    'backfill_formatter',
    'taxonomy_entity_index',
    'taxonomy',
    'lb_default_blocks_common_terms',
    'layout_builder',
  ];

  /**
   * UUID of created component.
   *
   * @var string
   */
  private $uuid;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->config('taxonomy_entity_index.settings')->set('types', [
      'node',
      'block_content',
    ])->save();
    $type = $this->enableDefaultBlockSupportForBlockType($this->createBlockContentType('text', 'Text'));
    $this->drupalCreateContentType(['type' => 'page']);
    FieldStorageConfig::create([
      'field_name' => 'term_reference',
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ])->save();
    FieldConfig::create([
      'field_name' => 'term_reference',
      'bundle' => 'page',
      'entity_type' => 'node',
    ])->save();
    FieldConfig::create([
      'field_name' => 'body',
      'bundle' => 'text',
      'entity_type' => 'block_content',
    ])->save();
    $display = \Drupal::service('entity_display.repository')->getViewDisplay('block_content', 'text', 'default');
    assert($display instanceof EntityViewDisplayInterface);
    $display->setComponent('body', [
      'type' => 'text_default',
    ])->save();
    $this->addTermReferenceFieldToBlockContentType($type);
  }

  /**
   * Tests the layout-builder default block functionality.
   */
  public function testDefaultBlock() {
    $vocab1 = $this->createVocabulary();
    $term = $this->createTerm($vocab1);
    $node = $this->drupalCreateNode([
      'type' => 'page',
      'status' => 1,
      'term_reference' => $term,
    ]);
    // A generic default with no position.
    $this->createBlockContent([
      'type' => 'text',
      'status' => 1,
      'info' => $this->randomMachineName(),
    ]);
    $position = $this->createLayoutPosition([
      'filterSettings' => [
        [
          'id' => 'block_type',
          'enabled' => TRUE,
          'weight' => 0,
          'settings' => [
            'block_types' => ['text'],
          ],
        ],
        [
          'id' => 'common_terms',
          'enabled' => TRUE,
          'weight' => 0,
          'settings' => [
            'vocabularies' => [
              $vocab1->id() => [
                'enabled' => TRUE,
                'weight' => 0,
              ],
            ],
            'backfill_mode' => BackFillTerms::MODE_MATCH_ALL,
          ],
        ],
      ],
    ]);

    $view_display = \Drupal::service('entity_display.repository')->getViewDisplay('node', 'page', 'default');
    assert($view_display instanceof EntityViewDisplayInterface);
    $view_display->setThirdPartySetting('layout_builder', 'enabled', TRUE);
    $view_display->save();
    $view_display = \Drupal::service('entity_display.repository')->getViewDisplay('node', 'page', 'default');
    assert($view_display instanceof LayoutBuilderEntityViewDisplay);
    $section = $view_display->getSection(0);
    assert($section instanceof Section);
    $this->uuid = \Drupal::service('uuid')->generate();
    $block_label = $this->randomMachineName();
    $component = (new SectionComponent($this->uuid, $section->getDefaultRegion(), [
      'id' => 'lb_default_blocks',
      'position' => $position->id(),
      'label_display' => 'visible',
      'label' => $block_label,
      'context_mapping' => ['entity' => 'layout_builder.entity'],
    ]));
    $section->appendComponent($component);
    $view_display->save();

    $this->drupalGet($node->toUrl());
    $assert = $this->assertSession();

    // When there are no matching defaults, the block should be hidden,
    // including the label.
    $assert->pageTextNotContains($block_label);

    $block = $this->createBlockContent([
      'type' => 'text',
      'body' => [
        'value' => 'some output',
        'format' => 'plain_text',
      ],
    ]);

    // Add a default.
    $position->setDefault($block->uuid());
    $position->save();
    // Should see default.
    $this->drupalGet($node->toUrl());
    $assert->pageTextContains($block_label);
    $assert->pageTextContains('some output');

    // A generic default.
    $this->createBlockContent([
      'type' => 'text',
      BlockContentTypeIntegration::FIELD_NAME => $position,
      'info' => $this->randomMachineName(),
      'body' => [
        'value' => 'This is a default that includes the [node:title]',
        'format' => 'plain_text',
      ],
    ]);

    $this->drupalGet($node->toUrl());
    $assert->pageTextContains($block_label);
    $assert->pageTextNotContains('This is a default that has no position');
    $assert->pageTextContains(sprintf('This is a default that includes the %s', $node->label()));

    // A contextual default.
    $this->createBlockContent([
      'type' => 'text',
      'term_reference' => [$term],
      BlockContentTypeIntegration::FIELD_NAME => $position,
      'info' => $this->randomMachineName(),
      'body' => [
        'value' => 'This is a contextual default that includes the [node:title]',
        'format' => 'plain_text',
      ],
    ]);

    $this->drupalGet($node->toUrl());
    $assert->pageTextNotContains('This is a default that has no position');
    $assert->pageTextNotContains(sprintf('This is a default that includes the %s', $node->label()));
    $assert->pageTextContains(sprintf('This is a contextual default that includes the %s', $node->label()));
  }

}
