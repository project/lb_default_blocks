<?php

namespace Drupal\Tests\lb_default_blocks\Functional;

use Drupal\lb_default_blocks\Hooks\BlockContentTypeIntegration;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\lb_default_blocks\Traits\BlockContentTestTrait;
use Drupal\Tests\lb_default_blocks\Traits\LayoutPositionTestTrait;

/**
 * Defines a class for testing defaults admin.
 *
 * @group lb_default_blocks
 */
class DefaultBlockAdminTest extends BrowserTestBase {

  use LayoutPositionTestTrait;
  use BlockContentTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'lb_default_blocks',
    'node',
    'block_content',
    'field',
    'filter',
    'views',
    'block',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalPlaceBlock('local_actions_block', ['region' => 'content']);
  }

  /**
   * Tests admin.
   */
  public function testDefaultAdmin() {
    $this->enableDefaultBlockSupportForBlockType($this->createBlockContentType('text', 'Text'));
    $this->assertThatOnlyAdminsCanAccessListing();
    $this->assertThatFiltersExist();
    $this->assertThatActionLinkExists();
    $this->assertThatPositionFilterWorks();
  }

  /**
   * Asserts admin listing permissions.
   */
  private function assertThatOnlyAdminsCanAccessListing() {
    $assert = $this->assertSession();
    $this->drupalGet('admin/content/default-blocks');
    $assert->statusCodeEquals(403);
    $this->drupalLogin($this->createUser([
      'administer blocks',
      'administer site configuration',
    ]));
    $this->drupalGet('admin/content/default-blocks');
  }

  /**
   * Asserts filters.
   */
  private function assertThatFiltersExist() {
    $assert = $this->assertSession();
    $assert->fieldExists('info');
    $assert->fieldExists('position');
  }

  /**
   * Asserts action links.
   */
  private function assertThatActionLinkExists() {
    $assert = $this->assertSession();
    $assert->linkExists('Add Text default block');
    $assert->linkByHrefExists('block/add/text?destination=admin/content/default-blocks');
  }

  /**
   * Assert position filter.
   */
  private function assertThatPositionFilterWorks() {
    $position1 = $this->createLayoutPosition();
    $position2 = $this->createLayoutPosition();

    $default1 = $this->createBlockContent([
      'type' => 'text',
      BlockContentTypeIntegration::FIELD_NAME => $position1,
      'info' => $this->randomMachineName(),
    ]);

    $default2 = $this->createBlockContent([
      'type' => 'text',
      BlockContentTypeIntegration::FIELD_NAME => $position2,
      'info' => $this->randomMachineName(),
    ]);

    $this->drupalGet('admin/content/default-blocks');
    $assert = $this->assertSession();
    $assert->pageTextContains($default1->label());
    $assert->pageTextContains($default2->label());
    $this->submitForm([
      'position' => $position1->id(),
    ], 'Apply');
    $assert->pageTextContains($default1->label());
    $assert->pageTextNotContains($default2->label());
  }

}
