<?php

declare(strict_types=1);

namespace Drupal\Tests\lb_default_blocks\Functional;

use Drupal\block_content\BlockContentTypeInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\lb_default_blocks\Traits\BlockContentTestTrait;

/**
 * Defines a class for testing block content type administration.
 *
 * @group lb_default_blocks
 */
class BlockContentTypeAdminTest extends BrowserTestBase {

  use BlockContentTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['lb_default_blocks', 'block_content', 'block'];

  /**
   * Tests block content type administration.
   */
  public function testBlockContentTypeAdmin(): void {
    $this->drupalLogin($this->drupalCreateUser([
      'administer blocks',
      'access administration pages',
      'access content',
    ]));
    $type = $this->createBlockContentType('text', 'Text');
    $this->drupalGet($type->toUrl('edit-form'));
    $assert = $this->assertSession();
    $assert->fieldExists('lb_default_blocks[enabled]');
    $this->submitForm([
      'lb_default_blocks[enabled]' => TRUE,
    ], 'Save');
    $type = \Drupal::entityTypeManager()->getStorage('block_content_type')->loadUnchanged('text');
    assert($type instanceof BlockContentTypeInterface);
    $this->assertTrue($type->getThirdPartySetting('lb_default_blocks', 'enabled', FALSE));
  }

}
