<?php

declare(strict_types=1);

namespace Drupal\Tests\lb_default_blocks\Traits;

use Drupal\block_content\BlockContentInterface;
use Drupal\block_content\BlockContentTypeInterface;
use Drupal\block_content\Entity\BlockContent;
use Drupal\block_content\Entity\BlockContentType;
use Drupal\Component\Utility\Random;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Defines a trait for block content functionality.
 */
trait BlockContentTestTrait {

  /**
   * Creates a block content type.
   *
   * @param string $id
   *   Block content type ID.
   * @param string|null $label
   *   Optional label.
   *
   * @return \Drupal\block_content\BlockContentTypeInterface
   *   Created block content type.
   */
  protected function createBlockContentType(string $id, ?string $label = NULL): BlockContentTypeInterface {
    $values = [
      'id' => $id,
      'label' => $label ?: (new Random())->name(),
      'revision' => TRUE,
    ];
    $bundle = BlockContentType::create($values);
    $bundle->save();
    if (method_exists($this, 'markEntityForCleanup')) {
      $this->markEntityForCleanup($bundle);
    }
    return $bundle;
  }

  /**
   * Enable layout-builder default block for the given block type.
   *
   * @param \Drupal\block_content\BlockContentTypeInterface $blockContentType
   *   Block type.
   *
   * @return \Drupal\block_content\BlockContentTypeInterface
   *   The block type.
   */
  protected function enableDefaultBlockSupportForBlockType(BlockContentTypeInterface $blockContentType): BlockContentTypeInterface {
    $blockContentType->setThirdPartySetting('lb_default_blocks', 'enabled', TRUE);
    $blockContentType->save();
    return $blockContentType;
  }

  /**
   * Adds a term reference field to the given block-type.
   *
   * @param \Drupal\block_content\BlockContentTypeInterface $blockContentType
   *   Block content type.
   * @param string $fieldName
   *   Field name.
   */
  protected function addTermReferenceFieldToBlockContentType(BlockContentTypeInterface $blockContentType, string $fieldName = 'term_reference'): void {
    FieldStorageConfig::create([
      'field_name' => $fieldName,
      'type' => 'entity_reference',
      'entity_type' => 'block_content',
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ])->save();
    FieldConfig::create([
      'field_name' => $fieldName,
      'bundle' => $blockContentType->id(),
      'entity_type' => 'block_content',
    ])->save();
  }

  /**
   * Creates a block content type.
   *
   * @param array $values
   *   Field values.
   *
   * @return \Drupal\block_content\BlockContentInterface
   *   Block content.
   */
  protected function createBlockContent(array $values): BlockContentInterface {
    if (!isset($values['type'])) {
      throw new \InvalidArgumentException('Missing block-content type.');
    }
    $block_content = BlockContent::create($values + [
      'info' => (new Random())->name(),
      'langcode' => 'en',
      'reusable' => TRUE,
      'status' => 1,
    ]);
    $block_content->save();
    if (method_exists($this, 'markEntityForCleanup')) {
      $this->markEntityForCleanup($block_content);
    }
    return $block_content;
  }

}
