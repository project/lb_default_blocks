<?php

namespace Drupal\Tests\lb_default_blocks\Traits;

use Drupal\Component\Utility\Random;
use Drupal\lb_default_blocks\Entity\LayoutPosition;
use Drupal\lb_default_blocks\Entity\LayoutPositionInterface;

/**
 * Defines a trait for testing layout position entities.
 */
trait LayoutPositionTestTrait {

  /**
   * Creates a layout position.
   *
   * @param array $values
   *   Field values.
   *
   * @return \Drupal\lb_default_blocks\Entity\LayoutPositionInterface
   *   New position.
   */
  protected function createLayoutPosition(array $values = []) : LayoutPositionInterface {
    $random = new Random();
    $position = LayoutPosition::create($values + [
      'id' => $random->name(),
      'name' => $random->name(),
      'description' => $random->name(),
    ]);
    $position->save();
    if (method_exists($this, 'markEntityForCleanup')) {
      $this->markEntityForCleanup($position);
    }
    return $position;
  }

}
