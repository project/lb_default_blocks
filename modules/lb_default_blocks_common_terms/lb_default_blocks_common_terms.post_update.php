<?php

/**
 * @file
 * Post update hooks.
 */

use Drupal\backfill_formatter\BackFillTerms;

/**
 * Update all the common_terms plugin and set default backfill mode.
 */
function lb_default_blocks_common_terms_post_update_set_backfill_mode(&$sandbox) {
  $positions = \Drupal::entityTypeManager()->getStorage('lb_default_blocks_position')->loadMultiple();
  array_walk($positions, function ($position) {
    $filter_config = array_map(function ($config) {
      if ($config['id'] === 'common_terms') {
        if (!isset($config['settings']['backfill_mode'])) {
          $config['settings']['backfill_mode'] = BackFillTerms::MODE_MATCH_ALL;
        }
      }
      return $config;
    }, $position->getSettings());
    $position->setSettings($filter_config);
    $position->save();
  });
}
