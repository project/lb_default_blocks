<?php

declare(strict_types=1);

namespace Drupal\lb_default_blocks_common_terms\Plugin\lb_default_blocks\Filter;

use Drupal\backfill_formatter\BackFillTerms;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\lb_default_blocks\Filter\DefaultBlockFilterBase;
use Drupal\taxonomy\VocabularyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a layout-builder default block matcher based on common terms.
 *
 * @DefaultBlockFilter(
 *   id="common_terms",
 *   label=@Translation("Common terms"),
 *   unconditional=FALSE,
 * )
 */
class CommonTerms extends DefaultBlockFilterBase {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Backfill terms.
   *
   * @var \Drupal\backfill_formatter\BackFillTerms
   */
  protected $backFillTerms;

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->backFillTerms = $container->get('backfill_formatter.backfill_terms');
    $instance->entityFieldManager = $container->get('entity_field.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + [
      'settings' => [
        'vocabularies' => [],
        'backfill_mode' => BackFillTerms::MODE_MATCH_ALL,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    $enabled_vocabulary_ids = [];
    foreach ($this->configuration['settings']['vocabularies'] as $vid => $detail) {
      if (empty($detail['enabled'])) {
        continue;
      }
      $enabled_vocabulary_ids[] = $vid;
    }
    if (!$enabled_vocabulary_ids) {
      return parent::calculateDependencies();
    }
    foreach ($this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple($enabled_vocabulary_ids) as $vocabulary) {
      $this->addDependency($vocabulary->getConfigDependencyKey(), $vocabulary->getConfigDependencyName());
    }
    return parent::calculateDependencies();
  }

  /**
   * {@inheritdoc}
   */
  public function onDependencyRemoval(array $dependencies): bool {
    $changed = parent::onDependencyRemoval($dependencies);
    foreach ($this->configuration['settings']['vocabularies'] as $vid => $detail) {
      if (empty($detail['enabled'])) {
        continue;
      }
      if (in_array('taxonomy.vocabulary.' . $vid, array_keys($dependencies['config']) ?? [], TRUE)) {
        // Disable this vocabulary.
        $this->configuration['settings']['vocabularies'][$vid]['enabled'] = FALSE;
        $changed = TRUE;
      }
    }
    return $changed;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $vocabularies = $this->getEligibleVocabularies();
    $build['vocabularies_blurb'] = [
      '#type' => 'html_tag',
      '#tag' => 'strong',
      '#value' => $this->t('Vocabularies'),
    ];
    if (!$vocabularies) {
      $build['no_vocabularies_blurb'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('There are no term reference fields on any block-content types, please add and configure an entity reference field that allows referencing taxonomy terms to at least one block-content type.'),
      ];
    }
    $build['vocabularies'] = [
      '#type' => 'table',
    ];
    $build['vocabularies']['#tabledrag'][] = [
      'action' => 'order',
      'relationship' => 'sibling',
      'group' => 'lb-default-block-vocabulary-weight',
    ];
    $weights = $this->configuration['settings']['vocabularies'];
    foreach ($vocabularies as $vid => $label) {
      $build['vocabularies'][$vid] = [
        '#weight' => $weights[$vid]['weight'] ?? 50,
        'enabled' => [
          '#title_display' => 'invisible',
          '#type' => 'checkbox',
          '#title' => $this->t('Enable for vocabulary %label', ['%label' => $label]),
          '#default_value' => !empty($weights[$vid]['enabled']),
        ],
        'label' => [
          '#plain_text' => $label,
        ],
        'weight' => [
          '#type' => 'weight',
          '#title' => $this->t('Weight for vocabulary %label', ['%label' => $label]),
          '#title_display' => 'invisible',
          '#delta' => 50,
          '#default_value' => $weights[$vid]['weight'] ?? 50,
          '#attributes' => [
            'class' => ['lb-default-block-vocabulary-weight'],
          ],
        ],
        '#attributes' => [
          'class' => ['draggable', 'js-form-wrapper'],
        ],
      ];
    }
    uasort($build['vocabularies'], [
      'Drupal\Component\Utility\SortArray',
      'sortByWeightProperty',
    ]);
    $build['vocabularies_description'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['fieldset__description'],
      ],
      'text' => [
        '#markup' => $this->t('Select the vocabularies that should influence the selection of common terms. Order the vocabularies in order of most relevant to least relevant.'),
      ],
    ];
    $build['backfill_mode'] = [
      '#type' => 'select',
      '#default_value' => $this->configuration['settings']['backfill_mode'],
      '#title' => $this->t('Back-fill Mode'),
      '#description' => $this->t('Select the back-fill mode used by Back-fill formatter module.</br>
        <em><b>Match all with Fallback</b></em> This mode backfills using all the matching terms with selected vocabulary.</br>
        <em><b>Match All</b></em> This mode backfills using all the matching terms with selected vocabulary.</br>
        <em><b>Match Any</b></em> This mode backfills using any matching terms with selected vocabulary.</br>'),
      '#options' => [
        BackFillTerms::MODE_MATCH_ALL_WITH_FALLBACK => 'Match all with Fallback',
        BackFillTerms::MODE_MATCH_ALL => 'Match All',
        BackFillTerms::MODE_MATCH_ANY => 'Match Any',
      ],
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['settings']['vocabularies'] = array_map(function (array $item) {
      $item['enabled'] = (bool) $item['enabled'];
      return $item;
    }, $form_state->getValue('vocabularies'));
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function primaryFilter(QueryInterface $query, ContentEntityInterface $contentEntity): void {
    $group = $query->orConditionGroup();
    foreach ($this->getEligibleFields() as $fieldName) {
      $group->exists($fieldName);
    }
    $query->condition($group);
  }

  /**
   * {@inheritdoc}
   */
  public function secondaryFilter(QueryInterface $query, ContentEntityInterface $contentEntity): void {
    parent::secondaryFilter($query, $contentEntity);
    $group = $query->andConditionGroup();
    foreach ($this->getEligibleFields() as $fieldName) {
      $group->notExists($fieldName);
    }
    $query->condition($group);
  }

  /**
   * {@inheritdoc}
   */
  public function alterMatches(array $results, QueryInterface $query, ContentEntityInterface $contentEntity): array {
    $vocabularies = $this->configuration['settings']['vocabularies'];
    $backfill_mode = ($this->configuration['settings']['backfill_mode']) ?: BackFillTerms::MODE_MATCH_ALL;
    $weights = array_map(function (array $item) {
      return $item['weight'];
    }, array_filter($vocabularies, function (array $item) {
      return !empty($item['enabled']);
    }));
    if ($vocabularies && $results && $matches = $this->backFillTerms->findMatchingEntitiesByTerms($contentEntity, 'block_content', [], 1, $weights, $backfill_mode, [], function (SelectInterface $query) use ($results) {
        // This will also limit target bundles if the block_type filter is
        // enabled for this position.
        $query->condition('entity_id', $results, 'IN');
    })) {
      return array_keys($matches);
    }
    return [];
  }

  /**
   * Gets the vocabularies that can be referenced from Block Content entities.
   *
   * @return array
   *   Array of vocabulary names, keyed by vid.
   */
  protected function getEligibleVocabularies(): array {
    $entity_reference_fields = $this->entityFieldManager->getFieldMapByFieldType('entity_reference');
    $vocabulary_ids = [];
    foreach (($entity_reference_fields['block_content'] ?? []) as $field_name => $details) {
      foreach ($details['bundles'] as $bundle) {
        $field_definitions = $this->entityFieldManager->getFieldDefinitions('block_content', $bundle);
        if (isset($field_definitions[$field_name]) && $field_definitions[$field_name]->getFieldStorageDefinition()->getSetting('target_type') === 'taxonomy_term') {
          $handler_settings = $field_definitions[$field_name]->getSetting('handler_settings');
          if (($handler_settings['target_bundles'] ?? NULL) === NULL) {
            // Any bundle is supported.
            return array_map(function (VocabularyInterface $vocabulary) {
              return $vocabulary->label();
            }, $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple());
          }
          $vocabulary_ids = array_merge($vocabulary_ids, $handler_settings['target_bundles']);
        }
      }
    }
    if (!$vocabulary_ids) {
      return [];
    }
    return array_map(function (VocabularyInterface $vocabulary) {
      return $vocabulary->label();
    }, $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple($vocabulary_ids));
  }

  /**
   * Gets the fields that can reference terms from Block Content entities.
   *
   * @return array
   *   Array of vocabulary names, keyed by vid.
   */
  protected function getEligibleFields(): array {
    $entity_reference_fields = $this->entityFieldManager->getFieldMapByFieldType('entity_reference');
    $fields = [];
    $vocabulary_ids = array_keys($this->configuration['settings']['vocabularies']);
    foreach (($entity_reference_fields['block_content'] ?? []) as $field_name => $details) {
      foreach ($details['bundles'] as $bundle) {
        $field_definitions = $this->entityFieldManager->getFieldDefinitions('block_content', $bundle);
        if (isset($field_definitions[$field_name]) && $field_definitions[$field_name]->getFieldStorageDefinition()->getSetting('target_type') === 'taxonomy_term') {
          $handler_settings = $field_definitions[$field_name]->getSetting('handler_settings');
          if (($handler_settings['target_bundles'] ?? NULL) === NULL) {
            // Any bundle is supported.
            $fields[] = $field_name;
            continue;
          }
          if (array_intersect($vocabulary_ids, $handler_settings['target_bundles'])) {
            $fields[] = $field_name;
          }
        }
      }
    }
    return array_unique($fields);
  }

}
