<?php

namespace Drupal\Tests\lb_default_blocks_common_terms\Kernel;

use Drupal\backfill_formatter\BackFillTerms;
use Drupal\lb_default_blocks\Hooks\BlockContentTypeIntegration;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\lb_default_blocks\Kernel\LbDefaultBlocksKernelTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Defines a class for testing default lookup with common terms.
 *
 * @group lb_default_blocks
 * @covers \Drupal\lb_default_blocks\Entity\LayoutPosition
 * @covers \Drupal\lb_default_blocks_common_terms\Plugin\lb_default_blocks\Filter\CommonTerms
 */
class LbDefaultBlocksLookupCommonTermsTest extends LbDefaultBlocksKernelTestBase {

  use TaxonomyTestTrait;
  use ContentTypeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'backfill_formatter',
    'taxonomy_entity_index',
    'taxonomy',
    'lb_default_blocks_common_terms',
  ];

  /**
   * {@inheritdoc}
   */
  protected function afterInitialSetup(): void {
    parent::afterInitialSetup();
    $this->config('taxonomy_entity_index.settings')->set('types', [
      'node',
      'block_content',
    ])->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();
    $this->installEntitySchema('taxonomy_term');
    $this->installSchema('taxonomy_entity_index', ['taxonomy_entity_index']);
    $type = $this->enableDefaultBlockSupportForBlockType($this->createBlockContentType('text', 'Text'));
    $this->createContentType(['type' => 'page']);
    FieldStorageConfig::create([
      'field_name' => 'term_reference',
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ])->save();
    FieldConfig::create([
      'field_name' => 'term_reference',
      'bundle' => 'page',
      'entity_type' => 'node',
    ])->save();
    $this->addTermReferenceFieldToBlockContentType($type);
  }

  /**
   * Tests block lookup.
   */
  public function testDefaultBlockLookup() {
    $vocab1 = $this->createVocabulary();
    $vocab2 = $this->createVocabulary();
    $position1 = $this->createLayoutPosition([
      'id' => 'position1',
      'filterSettings' => [
        [
          'id' => 'block_type',
          'enabled' => TRUE,
          'weight' => 0,
          'settings' => [
            'block_types' => ['text'],
          ],
        ],
        [
          'id' => 'common_terms',
          'enabled' => TRUE,
          'weight' => 0,
          'settings' => [
            'vocabularies' => [
              $vocab1->id() => [
                'enabled' => TRUE,
                'weight' => 0,
              ],
              $vocab2->id() => [
                'enabled' => FALSE,
                'weight' => 0,
              ],
            ],
            'backfill_mode' => BackFillTerms::MODE_MATCH_ALL,
          ],
        ],
      ],
    ]);
    $this->createLayoutPosition(['id' => 'position2']);
    $term1 = $this->createTerm($vocab1);
    $term2 = $this->createTerm($vocab1);
    $term3 = $this->createTerm($vocab2);
    // Note no contexts, items or position.
    $this->createBlockContent([
      'type' => 'text',
      'label' => $this->randomMachineName(),
    ]);
    $lookup = \Drupal::service('lb_default_blocks.lookup');

    $node = $this->createNode([
      'type' => 'page',
      'term_reference' => $term1,
    ]);

    // Red herrings (don't match on position, should not be found).
    $this->createBlockContent([
      'type' => 'text',
      'label' => $this->randomMachineName(),
      BlockContentTypeIntegration::FIELD_NAME => ['target_id' => 'position2'],
    ]);
    $this->createBlockContent([
      'type' => 'text',
      'label' => $this->randomMachineName(),
      BlockContentTypeIntegration::FIELD_NAME => ['target_id' => 'position2'],
      'term_reference' => [$term1],
    ]);
    // Red-herrings, not published.
    $this->createBlockContent([
      'type' => 'text',
      'status' => 0,
      'label' => $this->randomMachineName(),
      BlockContentTypeIntegration::FIELD_NAME => ['target_id' => 'position1'],
      'term_reference' => [$term1],
    ]);
    $this->createBlockContent([
      'type' => 'text',
      'status' => 0,
      'label' => $this->randomMachineName(),
      BlockContentTypeIntegration::FIELD_NAME => ['target_id' => 'position1'],
    ]);
    $this->createBlockContent([
      'type' => 'text',
      'label' => $this->randomMachineName(),
      BlockContentTypeIntegration::FIELD_NAME => ['target_id' => 'position1'],
      'term_reference' => [$term2],
    ]);
    $this->createBlockContent([
      'type' => 'text',
      'label' => $this->randomMachineName(),
      BlockContentTypeIntegration::FIELD_NAME => ['target_id' => 'position1'],
      'term_reference' => [$term3],
    ]);
    // Test that non matching items are not found.
    $this->assertLookup([], $lookup->lookupDefaultBlocksForEntity($position1, $node));

    // Test that we can match on context.
    $default = $this->createBlockContent([
      'type' => 'text',
      'label' => $this->randomMachineName(),
      BlockContentTypeIntegration::FIELD_NAME => ['target_id' => 'position1'],
      'term_reference' => [$term1],
    ]);
    $this->assertLookup([$default], $lookup->lookupDefaultBlocksForEntity($position1, $node));
  }

  /**
   * Test to ensure that lookup works fine with appropriate backfill mode.
   */
  public function testDefaultBlockLookupBackfillMode() {
    $vocab1 = $this->createVocabulary();
    $vocab2 = $this->createVocabulary();
    $position1 = $this->createLayoutPosition([
      'id' => 'position1',
      'filterSettings' => [
        [
          'id' => 'block_type',
          'enabled' => TRUE,
          'weight' => 0,
          'settings' => [
            'block_types' => ['text'],
          ],
        ],
        [
          'id' => 'common_terms',
          'enabled' => TRUE,
          'weight' => 0,
          'settings' => [
            'vocabularies' => [
              $vocab1->id() => [
                'enabled' => TRUE,
                'weight' => 0,
              ],
              $vocab2->id() => [
                'enabled' => TRUE,
                'weight' => 1,
              ],
            ],
            'backfill_mode' => BackFillTerms::MODE_MATCH_ALL_WITH_FALLBACK,
          ],
        ],
      ],
    ]);

    $term1 = $this->createTerm($vocab1);
    $term2 = $this->createTerm($vocab1);
    $term3 = $this->createTerm($vocab2);

    $node = $this->createNode([
      'type' => 'page',
      'term_reference' => [$term1, $term3],
    ]);

    $this->createBlockContent([
      'type' => 'text',
      'label' => $this->randomMachineName(),
      BlockContentTypeIntegration::FIELD_NAME => ['target_id' => 'position1'],
      'term_reference' => [$term2],
    ]);
    $block2 = $this->createBlockContent([
      'type' => 'text',
      'label' => $this->randomMachineName(),
      BlockContentTypeIntegration::FIELD_NAME => ['target_id' => 'position1'],
      'term_reference' => [$term3],
    ]);
    // Test that non matching items are not found.
    $lookup = \Drupal::service('lb_default_blocks.lookup');
    $this->assertLookup([$block2], $lookup->lookupDefaultBlocksForEntity($position1, $node));

    // Test that we can match on context.
    $default = $this->createBlockContent([
      'type' => 'text',
      'label' => $this->randomMachineName(),
      BlockContentTypeIntegration::FIELD_NAME => ['target_id' => 'position1'],
      'term_reference' => [$term1],
    ]);
    $this->assertLookup([$default], $lookup->lookupDefaultBlocksForEntity($position1, $node));

    $position2 = $this->createLayoutPosition([
      'id' => 'position2',
      'filterSettings' => [
        [
          'id' => 'block_type',
          'enabled' => TRUE,
          'weight' => 0,
          'settings' => [
            'block_types' => ['text'],
          ],
        ],
        [
          'id' => 'common_terms',
          'enabled' => TRUE,
          'weight' => 0,
          'settings' => [
            'vocabularies' => [
              $vocab1->id() => [
                'enabled' => TRUE,
                'weight' => 0,
              ],
              $vocab2->id() => [
                'enabled' => TRUE,
                'weight' => 1,
              ],
            ],
            'backfill_mode' => BackFillTerms::MODE_MATCH_ANY,
          ],
        ],
      ],
    ]);

    $node = $this->createNode([
      'type' => 'page',
      'term_reference' => [$term1, $term2],
    ]);

    $block1 = $this->createBlockContent([
      'type' => 'text',
      'label' => $this->randomMachineName(),
      BlockContentTypeIntegration::FIELD_NAME => ['target_id' => 'position1'],
      'term_reference' => [$term2],
    ]);
    $block2 = $this->createBlockContent([
      'type' => 'text',
      'label' => $this->randomMachineName(),
      BlockContentTypeIntegration::FIELD_NAME => ['target_id' => 'position1'],
      'term_reference' => [$term3],
    ]);
    // Test that non matching items are not found.
    $lookup = \Drupal::service('lb_default_blocks.lookup');
    $this->assertLookup([$block1], $lookup->lookupDefaultBlocksForEntity($position1, $node));
  }

  /**
   * Tests config dependencies.
   */
  public function testLayoutPositionDependencies() {
    $this->createBlockContent(['type' => 'text']);
    $vocab1 = $this->createVocabulary();
    $vocab2 = $this->createVocabulary();
    $position1 = $this->createLayoutPosition([
      'id' => 'position1',
      'default' => NULL,
      'filterSettings' => [
        [
          'id' => 'common_terms',
          'enabled' => TRUE,
          'weight' => 0,
          'settings' => [
            'vocabularies' => [
              $vocab1->id() => [
                'enabled' => TRUE,
                'weight' => 0,
              ],
              $vocab2->id() => [
                'enabled' => TRUE,
                'weight' => 0,
              ],
            ],
          ],
        ],
      ],
    ]);
    $dependencies = $position1->calculateDependencies()->getDependencies();
    $vocabs = [
      'taxonomy.vocabulary.' . $vocab1->id(),
      'taxonomy.vocabulary.' . $vocab2->id(),
    ];
    sort($vocabs);
    $this->assertEquals([
      'config' => $vocabs,
      'module' => ['lb_default_blocks_common_terms'],
    ], $dependencies);
    $vocab2->delete();
    $position1 = \Drupal::entityTypeManager()->getStorage('lb_default_blocks_position')->loadUnchanged($position1->id());
    $dependencies = $position1->calculateDependencies()->getDependencies();
    $this->assertEquals([
      'config' => [
        'taxonomy.vocabulary.' . $vocab1->id(),
      ],
      'module' => ['lb_default_blocks_common_terms'],
    ], $dependencies);
    $this->assertEquals([
      [
        'id' => 'common_terms',
        'enabled' => TRUE,
        'weight' => 0,
        'settings' => [
          'vocabularies' => [
            $vocab1->id() => [
              'enabled' => TRUE,
              'weight' => 0,
            ],
            $vocab2->id() => [
              'enabled' => FALSE,
              'weight' => 0,
            ],
          ],
        ],
      ],
    ], $position1->get('filterSettings'));
  }

}
