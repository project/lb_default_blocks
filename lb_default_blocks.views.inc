<?php

/**
 * @file
 * Contains views integration.
 */

/**
 * Implements hook_views_data_alter().
 */
function lb_default_blocks_views_data_alter(array &$data) {
  if (isset($data['block_content__lb_default_blocks__position']['lb_default_blocks__position_target_id'])) {
    $data['block_content__lb_default_blocks__position']['lb_default_blocks__position_target_id']['filter']['id'] = 'lb_default_blocks_position';
  }
}
