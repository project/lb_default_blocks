<?php

declare(strict_types=1);

namespace Drupal\lb_default_blocks;

use Drupal\block_content\BlockContentInterface;
use Drupal\block_content\BlockContentTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\lb_default_blocks\Hooks\BlockContentTypeIntegration;

/**
 * Defines a class for utility methods.
 */
final class Eligibility {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Constructs a new Utilities.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Checks if a block is an eligible default.
   *
   * @param \Drupal\block_content\BlockContentInterface $blockContent
   *   Block content.
   *
   * @return bool
   *   TRUE if supports layout-builder default blocks.
   */
  public function blockContentIsEligibleAsDefault(BlockContentInterface $blockContent): bool {
    $bundle = $blockContent->bundle();
    $blockContentType = $this->entityTypeManager->getStorage('block_content_type')->load($bundle);
    assert($blockContentType instanceof BlockContentTypeInterface);
    if (!$blockContentType->getThirdPartySetting('lb_default_blocks', 'enabled', FALSE)) {
      return FALSE;
    }
    return $blockContent->hasField('lb_default_blocks__position') && !$blockContent->get(BlockContentTypeIntegration::FIELD_NAME)->isEmpty() && $blockContent->{BlockContentTypeIntegration::FIELD_NAME}->entity;
  }

}
