<?php

declare(strict_types=1);

namespace Drupal\lb_default_blocks\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an annotation for layout-builder default block filter plugins.
 *
 * @Annotation
 */
class DefaultBlockFilter extends Plugin {

  /**
   * Plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * TRUE if the filter is applied unconditionally.
   *
   * @var bool
   */
  public $unconditional = FALSE;

}
