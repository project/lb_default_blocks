<?php

declare(strict_types=1);

namespace Drupal\lb_default_blocks\Filter;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\lb_default_blocks\Annotation\DefaultBlockFilter;

/**
 * Defines a plugin manager for layout-builder default block filter plugins.
 */
class FilterPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/lb_default_blocks/Filter', $namespaces, $module_handler, DefaultBlockFilterInterface::class, DefaultBlockFilter::class);
    $this->alterInfo('lb_default_blocks_filter');
    $this->setCacheBackend($cache_backend, 'lb_default_blocks_filter');
  }

}
