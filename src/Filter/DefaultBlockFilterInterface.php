<?php

declare(strict_types=1);

namespace Drupal\lb_default_blocks\Filter;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a class for a layout-builder default block filter plugin.
 *
 * Filters can be applied in one of two ways as follows:
 * - unconditional: this filter is applied on every pass
 * - conditional: this filter is applied once but can alter other queries
 * For each enabled conditional filter, the following occurs:
 * - the query is built, and filtered to matching positions and published blocks
 * - all unconditional filters are applied via their ::primaryFilter method
 * - the conditional filter's ::primaryFilter method is applied
 * - all previously applied conditional filters that didn't return results are
 *   given the opportunity to modify the query via their ::secondaryFilter
 *   method
 * - if there are results, the filter's ::alterMatches method is called,
 *   allowing it to perform other modifications to the matched block IDs before
 *   loading. These results are then returned.
 * - if there are no results, then the above steps are repeated for the next
 *   enabled conditional filter, in weight order.
 * This allows for e.g. attempting to find a block that specifically references
 * a particular piece of content, then if that fails, attempting to find a block
 * that has common terms and so on.
 *
 * @see \Drupal\lb_default_blocks\DefaultBlockLookup::lookupDefaultBlocksForEntity
 */
interface DefaultBlockFilterInterface extends ConfigurableInterface, PluginInspectionInterface, DependentPluginInterface, PluginFormInterface, PluginWithFormsInterface, ContainerFactoryPluginInterface {

  /**
   * Performs filtering of the query.
   *
   * If the plugin is unconditional, this is called for every attempt to match
   * blocks. If the plugin is conditional, this is called only once, when it is
   * this filter's turn (in weight order) to attempt to match blocks.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The query.
   * @param \Drupal\Core\Entity\ContentEntityInterface $contentEntity
   *   The content entity for which matches are sought.
   *
   * @see \Drupal\lb_default_blocks\DefaultBlockLookup::lookupDefaultBlocksForEntity
   */
  public function primaryFilter(QueryInterface $query, ContentEntityInterface $contentEntity): void;

  /**
   * TRUE if the filter is unconditional.
   *
   * Unconditional filters have their ::primaryFilter method called on each
   * attempt to match blocks. Conditional filters have their ::primaryFilter
   * method called only once, in weight order.
   *
   * @return bool
   *   TRUE if is unconditional.
   *
   * @see \Drupal\lb_default_blocks\DefaultBlockLookup::lookupDefaultBlocksForEntity
   */
  public function isUnconditional(): bool;

  /**
   * Performs secondary filtering of the query.
   *
   * Unconditional filters will not have this method called. Conditional filters
   * will have this method called if the attempt to match blocks that used this
   * filter did not yield any results and there are subsequent conditional
   * filters.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The query.
   * @param \Drupal\Core\Entity\ContentEntityInterface $contentEntity
   *   The content entity for which matches are sought.
   *
   * @see \Drupal\lb_default_blocks\DefaultBlockLookup::lookupDefaultBlocksForEntity
   */
  public function secondaryFilter(QueryInterface $query, ContentEntityInterface $contentEntity): void;

  /**
   * React to the removal of a dependency.
   *
   * @param array $dependencies
   *   Dependencies being removed.
   *
   * @return bool
   *   TRUE if changes were made to the plugin's configuration and the parent
   *   config entity should be resaved.
   */
  public function onDependencyRemoval(array $dependencies): bool;

  /**
   * Allows a conditional filter to alter results before they're loaded.
   *
   * Conditional filters have the opportunity to alter the results of the entity
   * query before the block content entities are loaded. This allows for other
   * filtering that cannot be achieved via an entity-query.
   *
   * @param array $results
   *   Array of block content IDs.
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The query used to retrieve the results.
   * @param \Drupal\Core\Entity\ContentEntityInterface $contentEntity
   *   The content entity for which layout-builder default block matches are
   *   sought.
   *
   * @return array
   *   Modified results as appropriate.
   */
  public function alterMatches(array $results, QueryInterface $query, ContentEntityInterface $contentEntity): array;

  /**
   * Gets the label of the plugin.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Plugin label
   */
  public function getLabel(): TranslatableMarkup;

  /**
   * Gets the plugin weight.
   *
   * @return bool
   *   Weight.
   */
  public function getWeight(): int;

}
