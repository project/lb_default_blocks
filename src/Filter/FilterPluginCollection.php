<?php

declare(strict_types=1);

namespace Drupal\lb_default_blocks\Filter;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Plugin\DefaultLazyPluginCollection;

/**
 * Defines a plugin collection for layout-builder default block filters.
 */
class FilterPluginCollection extends DefaultLazyPluginCollection {

  /**
   * {@inheritdoc}
   */
  public function __construct(PluginManagerInterface $manager, array $configurations = []) {
    $configurations = array_filter($configurations, function (array $item) {
      return !empty($item['enabled']);
    });
    parent::__construct($manager, $configurations);
  }

  /**
   * {@inheritdoc}
   */
  public function sortHelper($aID, $bID) {
    $a = $this->get($aID);
    $b = $this->get($bID);
    assert($a instanceof DefaultBlockFilterInterface);
    assert($b instanceof DefaultBlockFilterInterface);
    return $a->getConfiguration()['weight'] <=> $b->getConfiguration()['weight'];
  }

}
