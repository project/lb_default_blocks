<?php

declare(strict_types=1);

namespace Drupal\lb_default_blocks\Hooks;

use Drupal\block_content\BlockContentTypeInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class for managing block content type hooks.
 */
final class BlockContentTypeIntegration implements ContainerInjectionInterface {

  const FIELD_NAME = 'lb_default_blocks__position';

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Entity display respository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  private $entityDisplayRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * Constructs a new BlockContentTypeHooks.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entityDisplayRepository
   *   Entity display respository.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityDisplayRepositoryInterface $entityDisplayRepository) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityDisplayRepository = $entityDisplayRepository;
  }

  /**
   * React to block content type pre-save.
   *
   * @param \Drupal\block_content\BlockContentTypeInterface $blockContentType
   *   Block content type.
   */
  public function onPreSave(BlockContentTypeInterface $blockContentType): void {
    $originalState = $this->determineOriginalState($blockContentType);
    $newState = (bool) $blockContentType->getThirdPartySetting('lb_default_blocks', 'enabled', FALSE);
    if ($newState && !$originalState) {
      $this->addLayoutPositionField($blockContentType);
      return;
    }
    if (!$newState && $originalState) {
      $this->removeLayoutPositionField($blockContentType);
    }
  }

  /**
   * Adds a layout position field.
   *
   * @param \Drupal\block_content\BlockContentTypeInterface $blockContentType
   *   Block content type.
   */
  private function addLayoutPositionField(BlockContentTypeInterface $blockContentType): void {
    $field = $this->entityTypeManager->getStorage('field_config')->load(sprintf('block_content.%s.%s', $blockContentType->id(), self::FIELD_NAME));
    if (!$field) {
      $field_storage = $this->entityTypeManager->getStorage('field_storage_config')->load(sprintf('block_content.%s', self::FIELD_NAME));
      if (!$field_storage) {
        $field_storage = FieldStorageConfig::create([
          'entity_type' => 'block_content',
          'field_name' => self::FIELD_NAME,
          'type' => 'entity_reference',
          'locked' => TRUE,
          'settings' => [
            'target_type' => 'lb_default_blocks_position',
          ],
        ]);
        $field_storage->setTranslatable(FALSE);
        $field_storage->save();
      }

      $field = FieldConfig::create([
        'field_storage' => $field_storage,
        'bundle' => $blockContentType->id(),
        'label' => new TranslatableMarkup('Position'),
      ]);
      $field->setTranslatable(FALSE);
      $field->save();

      $display = $this->entityDisplayRepository->getFormDisplay('block_content', $blockContentType->id());
      $display->setComponent(self::FIELD_NAME, [
        'type' => 'options_select',
        'weight' => 5,
      ])->save();
    }
  }

  /**
   * Removes a layout position field.
   *
   * @param \Drupal\block_content\BlockContentTypeInterface $blockContentType
   *   Block content type.
   */
  private function removeLayoutPositionField(BlockContentTypeInterface $blockContentType): void {
    if ($field = $this->entityTypeManager->getStorage('field_config')->load(sprintf('block_content.%s.%s', $blockContentType->id(), self::FIELD_NAME))) {
      $field->delete();
    }
  }

  /**
   * Determines the original state of LB default block support for this type.
   *
   * @param \Drupal\block_content\BlockContentTypeInterface $blockContentType
   *   Block content type.
   *
   * @return bool
   *   TRUE if layout-builder default block support is enabled.
   */
  private function determineOriginalState(BlockContentTypeInterface $blockContentType): bool {
    if (!isset($blockContentType->original) || !($blockContentType->original instanceof BlockContentTypeInterface)) {
      return FALSE;
    }
    return (bool) $blockContentType->original->getThirdPartySetting('lb_default_blocks', 'enabled', FALSE);
  }

}
