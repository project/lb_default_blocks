<?php

namespace Drupal\lb_default_blocks;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\lb_default_blocks\Entity\LayoutPositionInterface;
use Drupal\lb_default_blocks\Filter\DefaultBlockFilterInterface;
use Drupal\lb_default_blocks\Hooks\BlockContentTypeIntegration;

/**
 * Defines a class for looking up layout-builder default blocks.
 */
class DefaultBlockLookup {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Constructs a new DefaultBlockLookup.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Lookup layout-builder default blocks for a given entity.
   *
   * @param \Drupal\lb_default_blocks\Entity\LayoutPositionInterface $position
   *   Layout position.
   * @param \Drupal\Core\Entity\ContentEntityInterface $contentEntity
   *   Entity.
   *
   * @return \Drupal\block_content\BlockContentInterface[]
   *   Matching blocks.
   */
  public function lookupDefaultBlocksForEntity(LayoutPositionInterface $position, ContentEntityInterface $contentEntity) : array {
    $filters = iterator_to_array($position->filters());
    $unconditional = array_filter($filters, function (DefaultBlockFilterInterface $filter) {
      return $filter->isUnconditional();
    });
    $staged = array_filter($filters, function (DefaultBlockFilterInterface $filter) {
      return !$filter->isUnconditional();
    });
    $missed = [];
    if (!$staged) {
      // There are only unconditional filters.
      $query = $this->defaultBlockQuery($position);
      foreach ($unconditional as $unconditional_filter) {
        assert($unconditional_filter instanceof DefaultBlockFilterInterface);
        $unconditional_filter->primaryFilter($query, $contentEntity);
      }
      return $this->queryResults($query, $contentEntity);
    }
    foreach ($staged as $filter) {
      assert($filter instanceof DefaultBlockFilterInterface);
      $query = $this->defaultBlockQuery($position);
      foreach ($unconditional as $unconditional_filter) {
        assert($unconditional_filter instanceof DefaultBlockFilterInterface);
        $unconditional_filter->primaryFilter($query, $contentEntity);
      }
      $filter->primaryFilter($query, $contentEntity);
      foreach ($missed as $missed_filter) {
        assert($missed_filter instanceof DefaultBlockFilterInterface);
        $missed_filter->secondaryFilter($query, $contentEntity);
      }
      if ($results = $this->queryResults($query, $contentEntity, $filter)) {
        return $results;
      }
      $missed[] = $filter;
    }
    // We got this far with the staged filters but got no results, do a generic
    // sweep now with only unconditional filters.
    $query = $this->defaultBlockQuery($position);
    foreach ($unconditional as $unconditional_filter) {
      assert($unconditional_filter instanceof DefaultBlockFilterInterface);
      $unconditional_filter->primaryFilter($query, $contentEntity);
    }
    // But with secondary exclusion from the conditional filters.
    foreach ($missed as $missed_filter) {
      assert($missed_filter instanceof DefaultBlockFilterInterface);
      $missed_filter->secondaryFilter($query, $contentEntity);
    }
    return $this->queryResults($query, $contentEntity);
  }

  /**
   * Generate a new layout-builder default block query.
   *
   * @param \Drupal\lb_default_blocks\Entity\LayoutPositionInterface $position
   *   Position.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   New query.
   */
  private function defaultBlockQuery(LayoutPositionInterface $position): QueryInterface {
    return $this->entityTypeManager->getStorage('block_content')->getQuery()
      ->condition(BlockContentTypeIntegration::FIELD_NAME, $position->id())
      ->condition('reusable', TRUE)
      ->condition('status', TRUE);
  }

  /**
   * Fetch default results.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   Query to execute.
   * @param \Drupal\Core\Entity\ContentEntityInterface $contentEntity
   *   The conditional filter used.
   * @param \Drupal\lb_default_blocks\Filter\DefaultBlockFilterInterface|null $filter
   *   The content entity for which layout-builder default blocks are sought.
   *
   * @return \Drupal\block_content\BlockContentInterface[]
   *   Loaded entities.
   */
  private function queryResults(QueryInterface $query, ContentEntityInterface $contentEntity, ?DefaultBlockFilterInterface $filter = NULL) : array {
    $results = $query->accessCheck(TRUE)->execute();
    if (empty($results)) {
      return [];
    }
    if ($filter) {
      $results = $filter->alterMatches($results, $query, $contentEntity);
    }
    return $this->entityTypeManager->getStorage('block_content')->loadMultiple($results);
  }

}
