<?php

declare(strict_types=1);

namespace Drupal\lb_default_blocks\EventSubscriber;

use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\Importer\MissingContentEvent;
use Drupal\Core\Config\StorageTransformEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\lb_default_blocks\Hooks\BlockContentTypeIntegration;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Defines an event subscriber for missing content dependencies.
 */
class MissingDefaultBlockSubscriber implements EventSubscriberInterface {

  const STATE_KEY = 'lb_default_blocks_missing_content';

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new MissingDefaultBlockSubscriber.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   State.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   */
  public function __construct(StateInterface $state, EntityTypeManagerInterface $entityTypeManager) {
    $this->state = $state;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Handles the missing content event.
   *
   * @param \Drupal\Core\Config\Importer\MissingContentEvent $event
   *   The missing content event.
   */
  public function onConfigImporterMissingContent(MissingContentEvent $event) {
    $defaults = $this->state->get(self::STATE_KEY, []);
    $block_storage = $this->entityTypeManager->getStorage('block_content');
    foreach (array_intersect_key($defaults, $event->getMissingContent()) as $item) {
      $entity = $block_storage->create($item);
      $entity->save();
      $event->resolveMissingContent($entity->uuid());
    }
    $this->state->set(self::STATE_KEY, []);
  }

  /**
   * Event subscriber for storage transform on import.
   *
   * Store the dependencies for layout positions.
   *
   * @param \Drupal\Core\Config\StorageTransformEvent $event
   *   Event.
   */
  public function onConfigImportStorageTransform(StorageTransformEvent $event) {
    $items = $event->getStorage()->listAll('lb_default_blocks.position.');
    $content_dependencies = [];
    foreach ($event->getStorage()->readMultiple($items) as $config_data) {
      foreach (array_merge($config_data['dependencies']['enforced']['content'] ?? [], $config_data['dependencies']['content'] ?? []) as $item) {
        [$entity_type, $bundle, $uuid] = explode(':', $item, 3);
        if ($entity_type !== 'block_content') {
          continue;
        }
        $content_dependencies[$uuid] = [
          'info' => sprintf('Default for layout position %s', $config_data['name']),
          'type' => $bundle,
          'uuid' => $uuid,
          BlockContentTypeIntegration::FIELD_NAME => ['target_id' => $config_data['id']],
        ];
      }
    }
    $this->state->set(self::STATE_KEY, $content_dependencies);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::STORAGE_TRANSFORM_IMPORT] = ['onConfigImportStorageTransform'];
    $events[ConfigEvents::IMPORT_MISSING_CONTENT] = ['onConfigImporterMissingContent'];
    return $events;
  }

}
