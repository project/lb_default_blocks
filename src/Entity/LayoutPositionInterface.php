<?php

namespace Drupal\lb_default_blocks\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\lb_default_blocks\Filter\FilterPluginCollection;

/**
 * Defines a class for a layout position entity.
 */
interface LayoutPositionInterface extends ConfigEntityInterface {

  /**
   * Gets the type description.
   *
   * @return string
   *   Description.
   */
  public function getDescription() : string;

  /**
   * Gets UUID of default value.
   *
   * @return string
   *   UUID of default value.
   */
  public function getDefault() : ?string;

  /**
   * Sets default UUID.
   *
   * @param string $default
   *   Default UUID.
   *
   * @return $this
   */
  public function setDefault(string $default): LayoutPositionInterface;

  /**
   * Gets the configured filters.
   *
   * @return \Drupal\lb_default_blocks\Filter\FilterPluginCollection
   *   Filters.
   */
  public function filters(): FilterPluginCollection;

  /**
   * Sets new settings.
   *
   * @param array $settings
   *   Settings.
   *
   * @return $this
   */
  public function setSettings(array $settings): LayoutPositionInterface;

  /**
   * Gets settings for position plugin.
   *
   * @return array
   */
  public function getSettings(): array;

}
