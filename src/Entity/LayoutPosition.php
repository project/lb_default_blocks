<?php

namespace Drupal\lb_default_blocks\Entity;

use Drupal\block_content\BlockContentInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\lb_default_blocks\Filter\DefaultBlockFilterInterface;
use Drupal\lb_default_blocks\Filter\FilterPluginCollection;

/**
 * Defines a class for a layout config-entity.
 *
 * @ConfigEntityType(
 *   id = "lb_default_blocks_position",
 *   label = @Translation("Layout position"),
 *   label_singular = @Translation("Layout position"),
 *   label_plural = @Translation("Layout positions"),
 *   label_collection = @Translation("Layout positions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count layout position",
 *     plural = "@count layout positions"
 *   ),
 *   handlers = {
 *     "access" = "\Drupal\lb_default_blocks\EntityHandlers\LayoutPositionAccessHandler",
 *     "list_builder" = "Drupal\lb_default_blocks\EntityHandlers\LayoutPositionListBuilder",
 *     "form" = {
 *       "default" = "\Drupal\lb_default_blocks\Form\LayoutPositionForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer lb default block position entities",
 *   config_prefix = "position",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/layout-positions/add",
 *     "delete-form" = "/admin/structure/layout-positions/manage/{lb_default_blocks_position}/delete",
 *     "reset-form" = "/admin/structure/layout-positions/manage/{lb_default_blocks_position}/reset",
 *     "overview-form" = "/admin/structure/layout-positions/manage/{lb_default_blocks_position}/overview",
 *     "edit-form" = "/admin/structure/layout-positions/manage/{lb_default_blocks_position}",
 *     "collection" = "/admin/structure/layout-positions",
 *   },
 *   config_export = {
 *     "name",
 *     "id",
 *     "description",
 *     "default",
 *     "filterSettings",
 *   }
 * )
 */
class LayoutPosition extends ConfigEntityBase implements LayoutPositionInterface, EntityWithPluginCollectionInterface {

  /**
   * The pane set type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * Name of the set type.
   *
   * @var string
   */
  protected $name;

  /**
   * Default entity UUID.
   *
   * @var string
   */
  protected $default;

  /**
   * Description of the layout position.
   *
   * @var string
   */
  protected $description = '';

  /**
   * Configured filters for this position.
   *
   * An associative array of filters assigned to this position, keyed by the
   * instance ID of each filter and using the properties:
   * - id: The plugin ID of the filter plugin instance.
   * - provider: The name of the provider that owns the recipient type.
   * - weight: Weight of the plugin
   * - enabled: (optional) TRUE if enabled
   * - settings: (optional) An array of configured settings for the filter.
   *
   * @var array
   */
  protected $filterSettings = [];

  /**
   * Holds the collection of filters that are attached to this position.
   *
   * @var \Drupal\Core\Plugin\DefaultLazyPluginCollection
   */
  protected $filterPluginCollection;

  /**
   * {@inheritdoc}
   */
  public function getDescription() : string {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefault() : ?string {
    return $this->default;
  }

  /**
   * {@inheritdoc}
   */
  public function setDefault(string $default): LayoutPositionInterface {
    $this->default = $default;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();
    if ($this->default && $default = \Drupal::service('entity.repository')->loadEntityByUuid('block_content', $this->default)) {
      assert($default instanceof BlockContentInterface);
      // @todo add an event listener to create empty versions of these.
      $this->addDependency($default->getConfigDependencyKey(), $default->getConfigDependencyName());
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function onDependencyRemoval(array $dependencies) {
    $changed = parent::onDependencyRemoval($dependencies);
    foreach ($this->filters() as $instance) {
      assert($instance instanceof DefaultBlockFilterInterface);
      $changed = $instance->onDependencyRemoval($dependencies) || $changed;
    }
    if ($this->default) {
      foreach (($dependencies['content'] ?? []) as $item) {
        if (preg_match(sprintf('/^block_content:.*:%s$/', preg_quote($this->default)), $item)) {
          $changed = TRUE;
          $this->default = NULL;
        }
      }
    }
    return $changed;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections(): array {
    return [
      'filterSettings' => $this->filters(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function filters(): FilterPluginCollection {
    if (!isset($this->filterPluginCollection)) {
      $this->filterPluginCollection = new FilterPluginCollection(\Drupal::service('plugin.manager.lb_default_blocks_filter'), $this->filterSettings);
    }
    return $this->filterPluginCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function setSettings(array $settings): LayoutPositionInterface {
    $this->filterSettings = $settings;
    unset($this->filterPluginCollection);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSettings(): array {
    return $this->filterSettings;
  }

}
