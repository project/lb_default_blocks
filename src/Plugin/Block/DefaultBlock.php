<?php

namespace Drupal\lb_default_blocks\Plugin\Block;

use Drupal\block_content\BlockContentInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Utility\Token;
use Drupal\lb_default_blocks\Entity\LayoutPositionInterface;
use Drupal\lb_default_blocks\DefaultBlockLookup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a block that shows layout-builder default blocks based on criteria.
 *
 * @Block(
 *   id = "lb_default_blocks",
 *   admin_label = @Translation("Default block"),
 *   context_definitions = {
 *     "entity" = @ContextDefinition("entity", label = "Layout entity")
 *   }
 * )
 */
class DefaultBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBin;

  /**
   * Static cache of lookup.
   *
   * @var array
   */
  protected $blocks = [];

  /**
   * Default block lookup.
   *
   * @var \Drupal\lb_default_blocks\DefaultBlockLookup
   */
  protected $defaultBlockLookup;

  /**
   * Token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  private $entityRepository;

  /**
   * Creates a new DefaultBlock.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\lb_default_blocks\DefaultBlockLookup $defaultBlockLookup
   *   Default block lookup.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Utility\Token $token
   *   Token service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   Entity field manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   Entity repository.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    DefaultBlockLookup $defaultBlockLookup,
    EntityTypeManagerInterface $entityTypeManager,
    Token $token,
    EntityFieldManagerInterface $entityFieldManager,
    CacheBackendInterface $cache,
    EntityRepositoryInterface $entityRepository
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->defaultBlockLookup = $defaultBlockLookup;
    $this->entityTypeManager = $entityTypeManager;
    $this->token = $token;
    $this->entityFieldManager = $entityFieldManager;
    $this->cacheBin = $cache;
    $this->entityRepository = $entityRepository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('lb_default_blocks.lookup'),
      $container->get('entity_type.manager'),
      $container->get('token'),
      $container->get('entity_field.manager'),
      $container->get('cache.lb_default_blocks'),
      $container->get('entity.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'position' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['position'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => $this->t('Position'),
      '#default_value' => $this->configuration['position'],
      '#options' => array_map(function (LayoutPositionInterface $position) {
        return $position->label();
      }, $this->entityTypeManager->getStorage('lb_default_blocks_position')->loadMultiple()),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['position'] = $form_state->getValue('position');
  }

  /**
   * Loads the layout position associated with this block.
   *
   * @return \Drupal\lb_default_blocks\Entity\LayoutPositionInterface|null
   *   The layout position
   */
  protected function getPosition(): ?LayoutPositionInterface {
    $position = $this->entityTypeManager->getStorage('lb_default_blocks_position')->load($this->configuration['position']);
    assert($position instanceof LayoutPositionInterface || is_null($position));
    return $position;
  }

  /**
   * Builds render array for default value.
   *
   * @param \Drupal\lb_default_blocks\Entity\LayoutPositionInterface $position
   *   Layout position.
   *
   * @return array
   *   Render array.
   */
  private function getDefaultAsRenderArray(LayoutPositionInterface $position): array {
    $cache = CacheableMetadata::createFromObject($position)->addCacheTags(['block_content_list']);
    $empty = [];
    $cache->applyTo($empty);
    if (!$position->getDefault()) {
      return $empty;
    }
    $item = $this->entityRepository->loadEntityByUuid('block_content', $position->getDefault());
    if (!$item) {
      return $empty;
    }
    assert($item instanceof BlockContentInterface);
    return $this->entityTypeManager->getViewBuilder('block_content')->view($item);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return array_merge(parent::getCacheTags(), ['block_content_list']);
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $context = $this->getContextValue('entity');
    $position = $this->getPosition();
    if (!$position) {
      return AccessResult::forbidden('Missing layout position')->addCacheTags(['lb_default_blocks_position_list']);
    }
    $lb_default_blocks = $this->doDefaultLookup($position, $context);
    if (empty($lb_default_blocks)) {
      if (empty($position->getDefault())) {
        $result = AccessResult::forbidden('No matching defaults')->addCacheableDependency($context)->addCacheTags(['block_content_list']);
        $result->addCacheableDependency($position);
        return $result;
      }
    }
    return parent::blockAccess($account);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $entity = $this->getContextValue('entity');
    $position = $this->getPosition();
    if (!$position) {
      return [
        '#cache' => [
          'tags' => ['lb_default_blocks_position_list'],
        ]
      ];
    }
    return $this->buildForEntityPositionAndVocabularies($entity, $position);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();
    $contexts[] = 'user.permissions';
    return $contexts;
  }

  /**
   * {@inheritdoc}
   */
  public function getPreviewFallbackString() {
    if ($position = $this->entityTypeManager->getStorage('lb_default_blocks_position')->load($this->configuration['position'])) {
      return $this->t('"@block" layout-builder default block (%position)', [
        '@block' => $this->label(),
        '%position' => $position->label(),
      ]);
    }
    return parent::getPreviewFallbackString();
  }

  /**
   * Builds layout-builder default block markup for entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Content entity.
   * @param \Drupal\lb_default_blocks\Entity\LayoutPositionInterface $position
   *   Position.
   *
   * @return array
   *   Render array
   */
  protected function buildForEntityPositionAndVocabularies(ContentEntityInterface $entity, LayoutPositionInterface $position) {
    $view_builder = $this->entityTypeManager->getViewBuilder('block_content');
    $cache = (new CacheableMetadata())
      ->addCacheTags([
        'block_content_list',
      ])->addCacheableDependency($position);
    $lb_default_blocks = $this->doDefaultLookup($position, $entity);
    // We only ever show a single block.
    $build = [];
    if (count($lb_default_blocks)) {
      $build = $view_builder->view(reset($lb_default_blocks));
    }
    $build['#token_data'] = $this->getTokenData($entity, $cache);
    $build['#post_render'][] = function ($html, array $elements) {
      return $this->token->replace($html, $elements['#token_data'], ['clear' => TRUE]);
    };
    $existing = CacheableMetadata::createFromRenderArray($build);
    $existing->merge($cache)->applyTo($build);
    $build['#access'] = !empty($lb_default_blocks);
    if (!$build['#access']) {
      $post_render = function ($html, array $elements) {
        return $this->token->replace($html, $elements['#token_data'], ['clear' => TRUE]);
      };
      $empty = [
        '#token_data' => $build['#token_data'],
        '#post_render' => [$post_render],
      ] + $this->getDefaultAsRenderArray($position);
      $default_value = [
        $empty + array_intersect_key($build, ['#cache' => TRUE]),
      ] + [
        '#contextual_links' => [
          sprintf('lb_default_blocks_%s_content', $entity->bundle()) => [
            'route_parameters' => [
              'lb_default_blocks_position' => $position->id(),
              $entity->getEntityTypeId() => $entity->id(),
            ],
          ],
        ],
      ];
      return $default_value;
    }
    return $build;
  }

  /**
   * Gets token data for entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity.
   * @param \Drupal\Core\Cache\CacheableMetadata $cache
   *   Cache.
   *
   * @return array
   *   Token data.
   */
  private function getTokenData(ContentEntityInterface $entity, CacheableMetadata $cache) : array {
    return [
      ($entity->getEntityType()->get('token_type') ?: $entity->getEntityTypeId()) => $entity,
    ];
  }

  /**
   * Do default lookup.
   *
   * @param \Drupal\lb_default_blocks\Entity\LayoutPositionInterface $position
   *   Position.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity to lookup for.
   *
   * @return \Drupal\block_content\BlockContentInterface[]
   *   Matching blocks.
   */
  protected function doDefaultLookup(LayoutPositionInterface $position, ContentEntityInterface $entity): array {
    $cid = $entity->id() . $position->id();
    if (isset($this->blocks[$cid])) {
      return $this->blocks[$cid];
    }
    if ($cached = $this->cacheBin->get($cid)) {
      return $cached->data;
    }
    $this->blocks[$cid] = $this->defaultBlockLookup->lookupDefaultBlocksForEntity($position, $entity);
    $this->cacheBin->set($cid, $this->blocks[$cid], Cache::PERMANENT, array_merge($entity->getCacheTags(), ['block_content_list'], $position->getCacheTags()));
    return $this->blocks[$cid];
  }

}
