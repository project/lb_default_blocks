<?php

declare(strict_types=1);

namespace Drupal\lb_default_blocks\Plugin\lb_default_blocks\Filter;

use Drupal\block_content\BlockContentTypeInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\lb_default_blocks\Filter\DefaultBlockFilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a layout-builder default block matcher based on block-type.
 *
 * @DefaultBlockFilter(
 *   id="block_type",
 *   label=@Translation("Block-type"),
 *   unconditional=TRUE
 * )
 */
class BlockType extends DefaultBlockFilterBase {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + ['settings' => ['block_types' => []]];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    if ($types = \Drupal::entityTypeManager()->getStorage('block_content_type')->loadMultiple($this->configuration['settings']['block_types'])) {
      foreach ($types as $type) {
        assert($type instanceof BlockContentTypeInterface);
        $this->addDependency($type->getConfigDependencyKey(), $type->getConfigDependencyName());
      }
    }
    return parent::calculateDependencies();
  }

  /**
   * {@inheritdoc}
   */
  public function primaryFilter(QueryInterface $query, ContentEntityInterface $contentEntity): void {
    if (!empty($this->configuration['settings']['block_types'])) {
      $query->condition('type', $this->configuration['settings']['block_types'], 'IN');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $build['block_types'] = [
      '#type' => 'checkboxes',
      '#default_value' => $this->configuration['settings']['block_types'],
      '#title' => $this->t('Target block-types'),
      '#description' => $this->t('Select the block types that can be shown in this position.'),
      '#options' => array_map(function (BlockContentTypeInterface $blockContentType) {
        return $blockContentType->label();
      }, array_filter($this->entityTypeManager->getStorage('block_content_type')->loadMultiple(), function (BlockContentTypeInterface $blockContentType) {
        return (bool) $blockContentType->getThirdPartySetting('lb_default_blocks', 'enabled', FALSE);
      })),
    ];
    if (empty($build['block_types']['#options'])) {
      $build['error'] = [
        '#type' => 'html_tag',
        '#tag' => 'strong',
        '#value' => new TranslatableMarkup('No block content types are enabled for layout-builder default block, please edit the block content types you wish to use and enable them.'),
      ];
    }
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['settings']['block_types'] = array_values(array_filter($form_state->getValue('block_types')));
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function onDependencyRemoval(array $dependencies): bool {
    $changed = parent::onDependencyRemoval($dependencies);
    foreach ($this->configuration['settings']['block_types'] as $delta => $type) {
      if (in_array('block_content.type.' . $type, array_keys($dependencies['config']) ?? [], TRUE)) {
        // Remove the item.
        unset($this->configuration['settings']['block_types'][$delta]);
        $changed = TRUE;
      }
    }
    if ($changed) {
      $this->configuration['settings']['block_types'] = array_values($this->configuration['settings']['block_types']);
    }
    return $changed;
  }

}
