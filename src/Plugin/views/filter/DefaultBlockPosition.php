<?php

declare(strict_types=1);

namespace Drupal\lb_default_blocks\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\lb_default_blocks\Entity\LayoutPositionInterface;
use Drupal\views\Plugin\views\filter\StringFilter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a views filter plugin for the position field.
 *
 * @ViewsFilter("lb_default_blocks_position")
 */
class DefaultBlockPosition extends StringFilter {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    parent::valueForm($form, $form_state);
    $form['value']['#type'] = 'select';
    unset($form['value']['#size']);
    $form['value']['#options'] = ['All' => $this->t('- Any -')] + array_map(function (LayoutPositionInterface $position) {
      return $position->label();
    }, $this->entityTypeManager->getStorage('lb_default_blocks_position')->loadMultiple());
  }

}
