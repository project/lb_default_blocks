<?php

namespace Drupal\lb_default_blocks\EntityHandlers;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines a class for a list builder for layout positions.
 */
class LayoutPositionListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    return [
      'name' => $this->t('Name'),
      'description' => $this->t('Description'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    return [
      'name' => $entity->toLink($entity->label(), 'edit-form'),
      'description' => $entity->getDescription(),
    ] + parent::buildRow($entity);
  }

}
