<?php

namespace Drupal\lb_default_blocks\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\lb_default_blocks\Entity\LayoutPosition;
use Drupal\lb_default_blocks\Entity\LayoutPositionInterface;
use Drupal\lb_default_blocks\Filter\DefaultBlockFilterInterface;
use Drupal\lb_default_blocks\Filter\FilterPluginCollection;
use Drupal\lb_default_blocks\Filter\FilterPluginManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class for a form for editing layout positions.
 */
class LayoutPositionForm extends EntityForm {

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  private $entityRepository;

  /**
   * Filter plugin manager.
   *
   * @var \Drupal\lb_default_blocks\Filter\FilterPluginManager
   */
  private $filterPluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory')->get('lb_default_blocks'),
      $container->get('messenger'),
      $container->get('entity_type.manager'),
      $container->get('entity.repository'),
      $container->get('plugin.manager.lb_default_blocks_filter')
    );
  }

  /**
   * Constructs a LayoutPositionForm.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   Entity repository.
   * @param \Drupal\lb_default_blocks\Filter\FilterPluginManager $filterPluginManager
   *   Filter plugin manager.
   */
  public function __construct(
    LoggerInterface $logger,
    MessengerInterface $messenger,
    EntityTypeManagerInterface $entityTypeManager,
    EntityRepositoryInterface $entityRepository,
    FilterPluginManager $filterPluginManager
  ) {
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entityTypeManager;
    $this->entityRepository = $entityRepository;
    $this->filterPluginManager = $filterPluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\lb_default_blocks\Entity\LayoutPositionInterface $layout_position */
    $layout_position = $this->entity;

    $form['#attached']['library'][] = 'lb_default_blocks/position_admin';

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $layout_position->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $layout_position->id(),
      '#machine_name' => [
        'exists' => [LayoutPosition::class, 'load'],
        'source' => ['name'],
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => !$layout_position->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#default_value' => $layout_position->getDescription(),
      '#description' => $this->t('Describe this Layout position. The text will be displayed on the <em>Layout positions</em> administration overview page.'),
      '#title' => $this->t('Description'),
    ];

    $default = NULL;
    if ($default_uuid = $layout_position->getDefault()) {
      $default = $this->entityRepository->loadEntityByUuid('block_content', $default_uuid);
    }
    $form['default'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'block_content',
      '#default_value' => $default,
      '#title' => $this->t('Default block'),
      '#description' => $this->t('Select the block to show by default. If you are deploying this layout position, be sure to account for creating a default block using an update hook with the matching UUID.'),
    ];

    $enabled_filters = $layout_position->filters();
    $all_filters = $this->filterPluginManager->getDefinitions();
    $form['filters_blurb'] = [
      '#type' => 'html_tag',
      '#tag' => 'strong',
      '#value' => $this->t('Enabled filters'),
    ];
    $form['enabled_filters'] = [
      '#type' => 'table',
    ];
    $form['enabled_filters']['#tabledrag'][] = [
      'action' => 'order',
      'relationship' => 'sibling',
      'group' => 'lb-default-block-filter-weight',
    ];
    $form['filter_settings'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Filter configuration'),
    ];
    foreach ($all_filters as $plugin_id => $definition) {
      $plugin = $this->getInstance($enabled_filters, $plugin_id);
      $form['enabled_filters'][$plugin_id] = [
        '#weight' => $plugin->getWeight(),
        'enabled' => [
          '#title_display' => 'invisible',
          '#type' => 'checkbox',
          '#attributes' => [
            'data-js-enable-filter' => $plugin_id,
          ],
          '#title' => $this->t('Enable filter %label', ['%label' => $definition['label']]),
          '#default_value' => $enabled_filters->has($plugin_id),
        ],
        'label' => [
          '#plain_text' => $this->t('@label (@conditional)', [
            '@label' => $definition['label'],
            '@conditional' => $plugin->isUnconditional() ? $this->t('Unconditional') : $this->t('Staged'),
          ]),
        ],
        'weight' => [
          '#type' => 'weight',
          '#title' => $this->t('Weight for filter %label', ['%label' => $definition['label']]),
          '#title_display' => 'invisible',
          '#delta' => 50,
          '#default_value' => $plugin->getWeight(),
          '#attributes' => [
            'class' => ['lb-default-block-filter-weight'],
          ],
        ],
        '#attributes' => [
          'class' => ['draggable', 'js-form-wrapper'],
        ],
      ];
      if ($plugin->hasFormClass('configure')) {
        $form['filterSettings']['settings'][$plugin_id] = [
          '#tree' => TRUE,
          '#type' => 'details',
          '#attributes' => [
            'data-js-configure-filter' => $plugin_id,
            'class' => array_filter([
              !$enabled_filters->has($plugin_id) ? 'visually-hidden' : NULL,
            ]),
          ],
          '#open' => TRUE,
          '#title' => $plugin->getLabel(),
          '#group' => 'filter_settings',
          '#parents' => ['filterSettings', $plugin_id, 'settings'],
        ];
        $subform_state = SubformState::createForSubform($form['filterSettings']['settings'][$plugin_id], $form, $form_state);
        $configurationForm = $plugin->buildConfigurationForm($form['filterSettings']['settings'][$plugin_id], $subform_state);
        $form['filterSettings']['settings'][$plugin_id] += $configurationForm;
      }
    }

    uasort($form['enabled_filters'], [
      'Drupal\Component\Utility\SortArray',
      'sortByWeightProperty',
    ]);

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    parent::copyFormValuesToEntity($entity, $form, $form_state);
    assert($entity instanceof LayoutPositionInterface);
    if (($block_id = $form_state->getValue('default')) && $block = $this->entityTypeManager->getStorage('block_content')->load($block_id)) {
      $entity->setDefault($block->uuid());
    }
    $filters = [];
    foreach ($form_state->getValue('enabled_filters') as $plugin_id => $details) {
      if (!empty($details['enabled'])) {
        $filters[$plugin_id] = [
          'enabled' => TRUE,
          'id' => $plugin_id,
          'weight' => $details['weight'],
          'settings' => $form_state->getValue([
            'filterSettings',
            $plugin_id,
            'settings',
          ]),
        ];
      }
    }
    $entity->setSettings($filters);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $layout_position = $this->entity;
    assert($layout_position instanceof LayoutPositionInterface);
    $filters = $layout_position->filters();
    foreach ($filters as $plugin_id => $plugin) {
      if ($plugin->hasFormClass('configure')) {
        $subform_state = SubformState::createForSubform($form['filterSettings']['settings'][$plugin_id], $form, $form_state);
        $plugin->validateConfigurationForm($form['filterSettings']['settings'][$plugin_id], $subform_state);
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $layout_position = $this->entity;
    assert($layout_position instanceof LayoutPositionInterface);
    $filters = $layout_position->filters();
    foreach ($filters as $plugin_id => $plugin) {
      if ($plugin->hasFormClass('configure')) {
        $subform_state = SubformState::createForSubform($form['filterSettings']['settings'][$plugin_id], $form, $form_state);
        $plugin->submitConfigurationForm($form['filterSettings']['settings'][$plugin_id], $subform_state);
      }
    }
    $status = $layout_position->save();

    $edit_link = $this->entity->toLink($this->t('Edit'), 'edit-form')->toString();
    $form_state->setRedirectUrl($layout_position->toUrl('collection'));
    if ($status == SAVED_UPDATED) {
      $this->messenger()->addStatus($this->t('Layout position %label has been updated.', ['%label' => $layout_position->label()]));
      $this->logger->notice('Layout position %label has been updated.', [
        '%label' => $layout_position->label(),
        'link' => $edit_link,
      ]);
      return;
    }
    $this->messenger()->addStatus($this->t('Layout position %label has been added.', ['%label' => $layout_position->label()]));
    $this->logger->notice('Layout position %label has been added.', [
      '%label' => $layout_position->label(),
      'link' => $edit_link,
    ]);
  }

  /**
   * Gets an instance of the plugin - either configured or default.
   *
   * @param \Drupal\lb_default_blocks\Filter\FilterPluginCollection $enabled_filters
   *   Plugin collection.
   * @param string $plugin_id
   *   Plugin ID.
   *
   * @return \Drupal\lb_default_blocks\Filter\DefaultBlockFilterInterface
   *   Instance for the given ID.
   */
  protected function getInstance(FilterPluginCollection $enabled_filters, string $plugin_id): DefaultBlockFilterInterface {
    if ($enabled_filters->has($plugin_id)) {
      return $enabled_filters->get($plugin_id);
    }
    $instance = $this->filterPluginManager->createInstance($plugin_id, []);
    assert($instance instanceof DefaultBlockFilterInterface);
    return $instance;
  }

}
